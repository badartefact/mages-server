﻿using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class RegisterRequest : PacketBase
    {
        [ProtoMember(1)]
        public string Key { get; set; }

        [ProtoMember(2)]
        public string Name { get; set; }

        public RegisterRequest()
        {
            Code = Enums.PacketClassCode.RegisterRequest;
        }
    }
}