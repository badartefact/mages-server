﻿using System.Collections.Generic;
using System.Linq;

namespace GameServer.Statistics
{
    public class ArenaGameStatisticManager
    {
        private Dictionary<string, ArenaPlayerStatistic> _players;

        public Dictionary<string, ArenaPlayerStatistic> Players
        {
            get { return _players; }
        }

        public ArenaPlayerStatistic this[string id]
        {
            get
            {
                if (_players != null && _players.ContainsKey(id))
                    return _players[id];
                else
                {
                    return ArenaPlayerStatistic.Empty;
                }
            }
        }

        public void AddPlayers(IEnumerable<string> players)
        {
            _players = players.ToDictionary(p => p, _ => new ArenaPlayerStatistic());
        }

        public void DamageOccured(string sender, string receiver, float damage)
        {
            if (_players.ContainsKey(sender))
            {
                _players[sender].Damage += damage;
            }
        }

        public string GetPlayerWithHighestDamage()
        {
            var maximumDamage = _players.Max(p => p.Value.Damage);
            return _players
                .Where(p => p.Value.Damage == maximumDamage)
                .Select(p => p.Key)
                .FirstOrDefault();
        }
    }
}