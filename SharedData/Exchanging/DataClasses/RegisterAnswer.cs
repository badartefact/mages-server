﻿using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class RegisterAnswer : PacketBase
    {
        [ProtoMember(1)]
        public bool CodeAccepted { get; set; }

        public RegisterAnswer()
        {
            Code = Enums.PacketClassCode.RegisterAnswer;
        }
    }
}