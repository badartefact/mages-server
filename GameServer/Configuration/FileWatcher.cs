﻿using System.IO;
using System.Threading;

namespace GameServer.Configuration
{
    public abstract class FileWatcher
    {
        private FileSystemWatcher _fileWatcher;

        public void StartWatch(string pathToFile)
        {
            _fileWatcher = new FileSystemWatcher
            {
                Filter = Path.GetFileName(pathToFile),
                Path = Path.GetDirectoryName(Path.GetFullPath(pathToFile)),
                NotifyFilter = NotifyFilters.LastWrite,
                EnableRaisingEvents = true
            };

            _fileWatcher.Changed += OnChanged;
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Changed)
            {
                _fileWatcher.EnableRaisingEvents = false;
                Thread.Sleep(300);
                Initialize();
                _fileWatcher.EnableRaisingEvents = true;
            }
        }

        public void Stop()
        {
            _fileWatcher.EnableRaisingEvents = false;
            _fileWatcher.Changed -= OnChanged;
        }

        protected abstract void Initialize();
    }
}