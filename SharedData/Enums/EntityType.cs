﻿namespace SharedData.Enums
{
    public enum EntityType
    {
        Character,
        Spell,
        SolidObject
    }
}