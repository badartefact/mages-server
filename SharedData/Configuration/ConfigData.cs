﻿using SharedData.Enums;
using System.Collections.Generic;

namespace SharedData.Configuration
{
    public class ConfigData
    {
        public Dictionary<SpellName, SpellData> Spells { get; set; }

        public Physics Physics { get; set; }

        public Level Level { get; set; }
    }
}