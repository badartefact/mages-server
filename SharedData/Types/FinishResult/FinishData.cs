﻿using ProtoBuf;
using System.Collections.Generic;

namespace SharedData.Types.FinishResult
{
    [ProtoContract]
    public class FinishData
    {
        [ProtoMember(1)]
        public string WinnerId { get; set; }

        [ProtoMember(2)]
        public Dictionary<string, PlayerFinishInfo> PlayersInfo { get; set; }
    }
}