using ProtoBuf;
using SharedData.Configuration;
using SharedData.Enums;
using System.Collections.Generic;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class GetConfigAnswer : PacketBase
    {
        [ProtoMember(1, IsRequired = true)]
        public Dictionary<SpellName, SpellData> Spells { get; set; }

        public GetConfigAnswer()
        {
            Code = PacketClassCode.GetMyPlayerConfigAnswer;
        }
    }
}
