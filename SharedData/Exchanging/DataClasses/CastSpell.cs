﻿using SharedData.Enums;
using SharedData.Types;
using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class CastSpell : PacketBase
    {
        [ProtoMember(1)]
        public TVector3 Target { get; set; }

        [ProtoMember(2)]
        public SpellName SpellName { get; set; }

        public CastSpell()
        {
            Code = PacketClassCode.CastSpell;
        }
    }
}