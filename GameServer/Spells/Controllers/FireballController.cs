﻿using BEPUphysics.CollisionRuleManagement;
using BEPUphysics.Entities.Prefabs;
using BEPUutilities;
using GameServer.Entities;
using GameServer.Enums;
using GameServer.Utils;
using SharedData.Enums;

namespace GameServer.Spells.Controllers
{
    public class FireballController : SpellController
    {
        public float Force { get; private set; }

        public float Damage { get; private set; }

        public float ExplodeDistance { get; private set; }

        private float _directSpeed;
        private float _flightDistance;
        private float _currentDistance;

        public FireballController(PlayerController controller)
            : base(ControllerEntityType.Fireball)
        {
            SpellName = SpellName.Fireball;
            CustomName = string.Format("Fireball_{0}", controller.Name);
            PlayerController = controller;
            Guid = System.Guid.NewGuid().ToString();
        }

        public override void Update()
        {
            _currentDistance += _directSpeed * Time.DeltaTime;

            if (_currentDistance > _flightDistance)
            {
                Destroy();
            }
        }

        public void Destroy()
        {
            CollisionRules.RemoveRule(PhysicsObject, PlayerController.MovementController.PhysicsObject);
            PlayerController.Space.Remove(PhysicsObject);
        }

        public void Send(Vector3 from, Vector3 target, float speed, float damage, float distance, float force, float explodeDistance)
        {
            PhysicsObject = new Sphere(from, 2, 1) { Tag = this };
            var inertia = PhysicsObject.LocalInertiaTensor;
            inertia.M11 = 0;
            inertia.M12 = 0;
            inertia.M13 = 0;
            inertia.M21 = 0;
            inertia.M22 = 0;
            inertia.M23 = 0;
            inertia.M31 = 0;
            inertia.M32 = 0;
            inertia.M33 = 0;
            PhysicsObject.LocalInertiaTensor = inertia;
            PhysicsObject.CollisionInformation.CollisionRules.Personal = CollisionRule.NoSolver;

            _directSpeed = speed;
            Damage = damage;
            _flightDistance = distance;
            Force = force;
            ExplodeDistance = explodeDistance;

            PhysicsObject.LookAt(target);

            var rawSpeed = (target - from);
            rawSpeed.Normalize();
            var targetSpeed = new Vector3(rawSpeed.X, 0, rawSpeed.Z);
            PhysicsObject.LinearVelocity = _directSpeed * targetSpeed;

            PlayerController.Space.Add(PhysicsObject);

            CollisionRules.AddRule(PhysicsObject, PlayerController.MovementController.PhysicsObject, CollisionRule.NoBroadPhase);
        }
    }
}