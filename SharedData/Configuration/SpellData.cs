﻿namespace SharedData.Configuration
{
    public class SpellData
    {
        public float Damage
        {
            get;
            set;
        }

        public float Duration
        {
            get;
            set;
        }

        public float Speed
        {
            get;
            set;
        }

        public float Distance
        {
            get;
            set;
        }

        public float SelfDamage
        {
            get;
            set;
        }

        public float Force
        {
            get;
            set;
        }

        public float CastingTime
        {
            get;
            set;
        }

        public float ReloadTime
        {
            get;
            set;
        }

        public float ExplodeDistance
        {
            get;
            set;
        }

        public float SelfForce
        {
            get;
            set;
        }

        public bool NeedTargeting
        {
            get;
            set;
        }

        public bool IsAttacking
        {
            get;
            set;
        }
    }
}