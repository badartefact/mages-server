﻿using EconomicServer.Data;
using EconomicServer.Lobby;
using GameServer;
using SharedData.Logger;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EconomicServer.Network
{
    internal class ArenaGameCreator
    {
        private readonly ArenaServer _arenaServer;
        private readonly ILogger _log;

        public ArenaGameCreator(ILogger log)
        {
            _arenaServer = new ArenaServer(log);
            _log = log;
        }

        public CreatedGameInfo CreateArenaGame(IEnumerable<PlayerController> players)
        {
            _log.Debug("Going to create arena game.");

            var playersInfo =
                    players.Select(p =>
                    {
                        var guid = Guid.NewGuid().ToString();
                        _log.Trace("Created guid: {0}, for: {1}", guid, p.Name);
                        return new Tuple<string, string, string>(guid, p.Id, p.Name);
                    }).ToList();

            var game = _arenaServer.StartGame(playersInfo);

            if (game == null) return null;

            var ip = "127.0.0.1";// "93.170.186.238";
            var port = game.Port;

            _log.Info("Game was created. Ip: {0}, Port: {1}.", ip, port);
            return new CreatedGameInfo(playersInfo, ip, port);
        }
    }
}