﻿using SharedData.Types;

namespace EconomicServer.Lobby
{
    public class PlayerController
    {
        public string Id { get; private set; }

        public string Name { get; private set; }

        public bool IsReadyToStartGame { get; set; }

        public bool IsConnectedToLobby { get; set; }

        public PlayerController(string id, string name)
        {
            Id = id;
            Name = name;
        }

        public PlayerGameData GetPlayerData()
        {
            var data = new PlayerGameData
            {
                Name = Name,
                IsConnectedToLobby = IsConnectedToLobby,
                IsReady = IsReadyToStartGame
            };
            return data;
        }
    }
}