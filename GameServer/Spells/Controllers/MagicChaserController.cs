﻿public class MagicChaserController
{
    /*
    private float _speed;
    private float _speedSlow;

    private float _damage;
    private float _force;

    private Vector3 _target;

    private float _currentTime;
    private float _duration;

    private float _rotationSpeed = 2f;

    private bool _firstTargetReached = false;
    private bool _enemyFounded = false;

    private Transform _enemy;

    private GameObject _sender;

    private bool _sended = false;

    void Update()
    {
        if (!_sended) return;

        CheckTime();

        if (_firstTargetReached)
        {
            if (!_enemyFounded)
            {
                FindEnemy();
            }
            else
            {
                _target = _enemy.position;
                Rotate();
            }
        }
        else
        {
            CheckFirstTarget();
        }
    }

    private void CheckFirstTarget()
    {
        float distance = Vector3.Distance(transform.position, _target);

        if (distance < 0.2f)
        {
            _firstTargetReached = true;
        }
    }

    private void CheckTime()
    {
        if (Time.time > _currentTime)
        {
            Destroy(gameObject);
        }
    }

    private void Rotate()
    {
        var newRotation = Quaternion.LookRotation(_target - transform.position, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, _rotationSpeed * Time.deltaTime);

        SetSpeed();
    }

    private void SetSpeed()
    {
        Vector3 speed = transform.forward * _speed;
        if (_enemyFounded)
        {
            _speed = Mathf.Lerp(_speed, _speedSlow, Time.deltaTime);
        }

        speed.y = 0;
        rigidbody.velocity = speed;
    }

    private void FindEnemy()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 15);
        foreach (Collider collider in hitColliders)
        {
            if ((collider.tag == "fighter") && _sender != collider.gameObject)
            {
                _enemy = collider.transform;
                _target = _enemy.position;
                _enemyFounded = true;
                break;
            }
        }
    }

    public void Send(Vector3 target, float speed, float damage, float force, float duration, GameObject sender)
    {
        _sender = sender;
        transform.LookAt(target);
        _target = target;
        _speed = speed;
        _speedSlow = _speed * 0.6f;

        _damage = damage;
        _force = force;
        _duration = duration;

        SetSpeed();
        _currentTime = Time.time + _duration;
        _sended = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if ((other.tag == "fighter") && _sender != other.gameObject)
        {
            Destroy(gameObject);
            Vector3 direction = (other.transform.position - transform.position).normalized;
            ImpactObject impact = new ImpactObject(direction, _force);
            other.SendMessage("AddDamage", _damage);
            other.SendMessage("AddImpact", impact);
        }

        if (other.tag == "fightmagic")
        {
            Destroy(gameObject);
            Destroy(other.gameObject);
        }

        if (other.tag == "wall")
        {
            Destroy(gameObject);
        }
    }*/
}