﻿using System.Collections.Generic;
using System.Linq;

namespace GameServer.Utils
{
    internal class PortHandler
    {
        private readonly Dictionary<int, bool> _ports;

        public PortHandler()
        {
            _ports = new Dictionary<int, bool>();
            FillWithAvailablePorts();
        }

        private void FillWithAvailablePorts()
        {
            Enumerable.Range(33001, 50).ToList().ForEach(num => _ports.Add(num, true));
        }

        public bool SomePortAvailable()
        {
            return _ports.Any(p => p.Value);
        }

        public int GetPort()
        {
            var result = _ports.Where(p => p.Value).Select(p => p.Key).FirstOrDefault();
            if (result != 0)
            {
                _ports[result] = false;
                return result;
            }

            return 0;
        }

        public void ReturnPort(int port)
        {
            if (_ports.ContainsKey(port))
            {
                _ports[port] = true;
            }
        }
    }
}