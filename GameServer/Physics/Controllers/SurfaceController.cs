﻿using GameServer.Configuration;
using System.Timers;

namespace GameServer.Physics.Controllers
{
    public class SurfaceController
    {
        #region Private members

        private Timer _updateTimer;

        #endregion Private members

        #region Public members

        public float Radius { get; private set; }

        #endregion Public members

        #region Constructor

        public SurfaceController()
        {
            InitTimer();
            SetDefaultValues();
        }

        #endregion Constructor

        #region Public methods

        public void Start()
        {
            _updateTimer.Start();
        }

        public void Stop()
        {
            _updateTimer.Stop();
        }

        public void Reset()
        {
            SetDefaultValues();
            RestartTimer();
        }

        #endregion Public methods

        #region Private members

        private void SetDefaultValues()
        {
            Radius = ConfigurationManager.Instance.Data.Level.GroundStartRadius;
        }

        private void RestartTimer()
        {
            _updateTimer.Stop();
            _updateTimer.Start();
        }

        private void InitTimer()
        {
            _updateTimer = new Timer { Interval = ConfigurationManager.Instance.Data.Level.GroundUpdateInterval };
            _updateTimer.Elapsed += UpdateTimerElapsed;
        }

        private void UpdateTimerElapsed(object sender, ElapsedEventArgs e)
        {
            float updateValue = ConfigurationManager.Instance.Data.Level.GroundUpdateValue;
            Radius = Radius <= updateValue ? 0 : Radius -= updateValue;
        }

        #endregion Private members
    }
}