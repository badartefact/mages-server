﻿using SharedData.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EconomicServer.Lobby
{
    public class GameController
    {
        public Guid Id { get; set; }

        public int MaximumAmount { get; set; }

        public int CurrentAmount
        {
            get
            {
                int count = 0;
                if (_players != null)
                {
                    count = _players.Count;
                }
                return count;
            }
        }

        public string Name { get; set; }

        public string Comment { get; set; }

        private readonly List<PlayerController> _players;

        public List<PlayerController> Players
        {
            get
            {
                return _players;
            }
        }

        public IEnumerable<ArenaGameData> GetData(IEnumerable<GameController> games)
        {
            return games.Select(controller => GetData()).ToList();
        }

        public ArenaGameData GetData()
        {
            return new ArenaGameData
            {
                Id = Id,
                Comment = Comment,
                Name = Name,
                Players = GetPlayersData(),
                CurrentAmount = CurrentAmount,
                MaximumAmount = MaximumAmount
            };
        }

        private List<PlayerGameData> GetPlayersData()
        {
            return _players.Select(p => p.GetPlayerData()).ToList();
        }

        public GameController()
        {
            _players = new List<PlayerController>();
        }

        public void AddPlayer(PlayerController lobbyPlayer)
        {
            _players.Add(lobbyPlayer);
        }

        public void RemovePlayerFromLobby(PlayerController lobbyPlayer)
        {
            if (_players.Contains(lobbyPlayer))
            {
                _players.Remove(lobbyPlayer);
            }
        }

        public void RemovePlayerFromLobby(string id)
        {
            var lobbyPlayer = _players.FirstOrDefault(p => p.Id == id);
            if (lobbyPlayer != null)
            {
                _players.Remove(lobbyPlayer);
            }
        }
    }
}