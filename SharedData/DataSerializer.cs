﻿using ProtoBuf;
using System;
using System.IO;

namespace SharedData
{
    public static class DataSerializer<T>
    {
        public static byte[] GetBytes(T target)
        {
            byte[] result = null;

            using (var stream = new MemoryStream())
            {
                Serializer.Serialize(stream, target);
                result = stream.ToArray();
            }

            return result;
        }

        public static T Get(Stream stream)
        {
            return Serializer.Deserialize<T>(stream);
        }

        public static T Get(byte[] bytes)
        {
            try
            {
                var data = new MemoryStream(bytes);
                data.SetLength(bytes.Length);
                return Get(data);
            }
            catch (Exception ex)
            {
                using (var file = new StreamWriter("log.txt", true))
                {
                    file.WriteLine("{0} {1}", DateTime.Now, ex.Message);
                    file.WriteLine("{0} {1}", DateTime.Now, ex.StackTrace);
                }
                return default(T);
            }
        }
    }
}