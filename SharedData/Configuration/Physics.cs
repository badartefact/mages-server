﻿namespace SharedData.Configuration
{
    public class Physics
    {
        public float ImpactConsumeSpeed { get; set; }

        public float MovementSpeed { get; set; }

        public float RotationSpeed { get; set; }

        public float MovementStopDistance { get; set; }
    }
}