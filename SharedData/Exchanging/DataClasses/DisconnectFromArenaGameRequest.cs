﻿using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class DisconnectFromArenaGameRequest : PacketBase
    {
        public DisconnectFromArenaGameRequest()
        {
            Code = Enums.PacketClassCode.DisconnectFromLobbyRequest;
        }
    }
}