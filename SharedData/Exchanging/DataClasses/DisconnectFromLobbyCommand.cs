﻿using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class DisconnectFromLobbyCommand : PacketBase
    {
        public DisconnectFromLobbyCommand()
        {
            Code = Enums.PacketClassCode.DisconnectFromLobbyRequest;
        }
    }
}