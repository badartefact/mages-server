﻿using BEPUphysics.Entities;
using GameServer.Entities;
using GameServer.Enums;
using SharedData.Enums;
using SharedData.Types;

namespace GameServer.Spells.Controllers
{
    public class SpellController : BaseEntityController
    {
        public SpellController(ControllerEntityType entityType)
            : base(entityType)
        {
        }

        public string Guid { get; protected set; }

        public TVector3 Position
        {
            get
            {
                return new TVector3
                {
                    X = PhysicsObject.Position.X,
                    Y = PhysicsObject.Position.Y,
                    Z = PhysicsObject.Position.Z
                };
            }
        }

        public TQuaternion Rotation
        {
            get
            {
                return new TQuaternion
                {
                    X = PhysicsObject.Orientation.X,
                    Y = PhysicsObject.Orientation.Y,
                    Z = PhysicsObject.Orientation.Z,
                    W = PhysicsObject.Orientation.W
                };
            }
        }

        public string CustomName { get; protected set; }

        public SpellName SpellName { get; protected set; }

        public PlayerController PlayerController { get; protected set; }

        public Entity PhysicsObject { get; protected set; }

        public virtual void Update()
        {
        }
    }
}