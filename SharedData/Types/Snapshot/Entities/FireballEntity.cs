﻿using SharedData.Enums;
using ProtoBuf;

namespace SharedData.Types.Snapshot.Entities
{
    [ProtoContract]
    public class FireballEntity : SpellEntity
    {
        public FireballEntity()
        {
            SpellName = SpellName.Fireball;
        }
    }
}