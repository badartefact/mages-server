﻿using SharedData.Enums;
using SharedData.Types;
using SharedData.Types.FinishResult;
using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class FinishDataAnswer : PacketBase
    {
        [ProtoMember(1)]
        public FinishData FinishData;

        public FinishDataAnswer()
        {
            Code = PacketClassCode.FinishData;
        }
    }
}