using GameServer.Configuration;
using GameServer.Game;
using GameServer.Utils;
using SharedData.Logger;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace GameServer
{
    public class ArenaServer
    {
        private readonly object _lock = new object();
        private readonly ILogger _log;
        private Thread _updater;
        private readonly Stopwatch _stopWatch;
        private readonly List<ArenaGameController> _games;
        private readonly PortHandler _portHandler;

        private const int SendInterval = 15;

        public ArenaServer(ILogger log)
        {
            _log = log;
            JitCompilation.ForceLoadAll(System.Reflection.Assembly.GetExecutingAssembly());
            _stopWatch = new Stopwatch();
            _games = new List<ArenaGameController>();
            _portHandler = new PortHandler();

            InitUpdater();
        }

        private void InitUpdater()
        {
            _updater = new Thread(Update);
            _updater.Start();
        }

        public ArenaGameController StartGame(IEnumerable<Tuple<string, string, string>> playersInfo)
        {
            lock (_lock)
            {
                try
                {
                    if (!_portHandler.SomePortAvailable())
                    {
                        _log.Error("Cannot create game, coz there no available ports!");
                        return null;
                    }

                    var config = new ArenaGameConfig
                    {
                        Port = _portHandler.GetPort(),
                        MaxDamage = 20000,
                        MaxRounds = 333
                    };

                    _log.Debug("Going to use port: '{0}'.", config.Port);
                    var arenaGame = new ArenaGameController(_log, config);

                    arenaGame.Init(playersInfo);

                    _games.Add(arenaGame);
                    arenaGame.OnAllDisconnected += GameOnAllDisconnected;
                    arenaGame.OnGameEnded += GameOnAllDisconnected;
                    _log.Info("Game  was added to arena server.");
                    return arenaGame;
                }
                catch (Exception ex)
                {
                    _log.Error("Cannot create game. {0}", ex);
                }
            }
            return null;
        }

        private void GameOnAllDisconnected(ArenaGameController game)
        {
            game.Shutdown();
            _portHandler.ReturnPort(game.Port);
            RemoveGame(game);
        }

        private void RemoveGame(ArenaGameController game)
        {
            game.OnAllDisconnected -= GameOnAllDisconnected;

            lock (_lock)
            {
                if (_games.Contains(game))
                {
                    _games.Remove(game);
                    _log.Info("Game  was removed from arena server.");
                }
            }
        }

        private void Update()
        {
            while (true)
            {
                Thread.Sleep(SendInterval);

                _stopWatch.Stop();

                Time.DeltaTime = _stopWatch.ElapsedMilliseconds / 1000f;

                lock (_lock)
                {
                    var games = new List<ArenaGameController>(_games);
                    foreach (var game in games)
                    {
                        game.Update();
                    }
                }

                _stopWatch.Restart();
            }
        }
    }
}
