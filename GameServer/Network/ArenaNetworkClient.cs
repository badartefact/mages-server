using Lidgren.Network;
using SharedData;
using SharedData.Enums;
using SharedData.Exchanging;
using SharedData.Exchanging.DataClasses;
using SharedData.Logger;
using SharedData.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace GameServer.Network
{
    public class ArenaNetworkClient
    {
        private const string ConfigName = "ARENA";

        private NetServer _server;
        private PacketDataManager _packetManager;

        private readonly int _port = 34001;
        private readonly ILogger _log;
        private readonly List<Tuple<string, string>> _playerCodes;
        private readonly Dictionary<string, NetConnection> _connectedPlayers;

        public event Action<string, TVector3> OnMoveTo = delegate { };

        public event Action<string, SpellName, TVector3> OnCastSpell = delegate { };

        public event Action<string> OnPlayerConnected = delegate { };

        public event Action<string> OnPlayerDisconnected = delegate { };

        public ArenaNetworkClient(ILogger log, int port, List<Tuple<string, string>> playerCodes)
        {
            _port = port;
            _log = log;
            _playerCodes = playerCodes;
            _connectedPlayers = new Dictionary<string, NetConnection>();

            InitPacketManager();
            InitNetworkClient();
        }

        private void InitNetworkClient()
        {
            var config = new NetPeerConfiguration(ConfigName) { Port = _port };
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            _server = new NetServer(config);
            var context = new SynchronizationContext();
            _server.RegisterReceivedCallback(GotMessage, context);
            _server.Start();
        }

        private void InitPacketManager()
        {
            _packetManager = new PacketDataManager();
            _packetManager.OnNewDataFromSocket += OnNewDataFromSocket;
        }

        private void OnNewDataFromSocket(object sender, byte[] data, int protoClass, object status)
        {
            var connection = status as NetConnection;
            if (connection == null)
            {
                _log.Warn("Received 'null' connection from DataPacketManager.");
                return;
            }

            var playerId = GetPlayerIdByConnection(connection);
            if (playerId == null)
            {
                _log.Warn("Received data for player that doesn't exist. ProtoClass: '{0}'", protoClass);
                return;
            }

            var request = (PacketClassCode)protoClass;
            switch (request)
            {
                case PacketClassCode.MoveTo:
                    _log.Trace("Received 'move to' command: '{0}'", protoClass);
                    var moveToCommand = DataSerializer<MoveTo>.Get(data);
                    OnMoveTo(playerId, moveToCommand.Position);
                    break;

                case PacketClassCode.CastSpell:
                    _log.Trace("Received 'cast spell' command: '{0}'", protoClass);
                    var castSpellCommand = DataSerializer<CastSpell>.Get(data);
                    OnCastSpell(playerId, castSpellCommand.SpellName, castSpellCommand.Target);
                    break;
                    break;

                default:
                    _log.Warn("Received unknown command: '{0}'", protoClass);
                    break;
            }
        }

        private string GetPlayerIdByConnection(NetConnection connection)
        {
            return _connectedPlayers.Where(p => p.Value == connection).Select(p => p.Key).FirstOrDefault();
        }

        private void GotMessage(object state)
        {
            var message = _server.ReadMessage();

            switch (message.MessageType)
            {
                case NetIncomingMessageType.ConnectionApproval:
                    _log.Debug("Someone connected.");
                    var code = message.ReadString();

                    var player = _playerCodes.FirstOrDefault(p => p.Item1 == code);

                    if (player == null)
                    {
                        _log.Debug("Wrong code: '{0}'.", code);
                        message.SenderConnection.Deny("Fuck you, сучара позорная!");
                    }
                    else
                    {
                        _log.Debug("Code '{0}' is ok and belongs to player with id: '{1}'.", code, player.Item2);
                        message.SenderConnection.Approve();
                        _connectedPlayers.Add(player.Item2, message.SenderConnection);
                    }

                    break;

                case NetIncomingMessageType.Data:
                    var buffer = new byte[1024];

                    message.ReadBytes(buffer, 0, 2);
                    var packetSize = PacketDataManager.GetPacketLength(buffer);
                    message.ReadBytes(buffer, 2, packetSize - 2);
                    _packetManager.Parse(buffer, message.SenderConnection);
                    break;

                case NetIncomingMessageType.StatusChanged:

                    var status = message.SenderConnection.Status;
                    if (status == NetConnectionStatus.Connected)
                    {
                        var playerId = GetPlayerIdByConnection(message.SenderConnection);
                        OnPlayerConnected(playerId);
                    }

                    if (status == NetConnectionStatus.Disconnected)
                    {
                        var playerId = GetPlayerIdByConnection(message.SenderConnection);
                        OnPlayerDisconnected(playerId);
                    }
                    break;

                default:
                    _log.Debug("Unhandled message: {0}", message.MessageType);
                    break;
            }
        }

        public void SendToAll(PacketBase packet, NetDeliveryMethod netDeliveryMethod = NetDeliveryMethod.ReliableOrdered)
        {
            var message = CreateMessage(packet);
            _server.SendToAll(message, netDeliveryMethod);
        }

        public void Send(PacketBase packet, IEnumerable<string> idList, NetDeliveryMethod netDeliveryMethod = NetDeliveryMethod.ReliableOrdered, int channel = 0)
        {
            var message = CreateMessage(packet);
            var connections = GetConnectionsById(idList);
            if (connections.Count > 0)
            {
                _server.SendMessage(message, connections, NetDeliveryMethod.UnreliableSequenced, 0);
            }
        }

        private List<NetConnection> GetConnectionsById(IEnumerable<string> idList)
        {
            return idList.Where(id => _connectedPlayers.ContainsKey(id)).Select(id => _connectedPlayers[id]).ToList();
        }

        private NetOutgoingMessage CreateMessage(PacketBase packet)
        {
            var message = _server.CreateMessage();
            var data = _packetManager.GetBytes(packet);
            message.Write(data);
            return message;
        }

        public void Shutdown()
        {
            _server.Shutdown("bye");
        }
    }
}
