﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Configuration
{
    public class ArenaGameConfig
    {
        public int Port { get; set; }

        public int MaxRounds { get; set; }

        public int MaxDamage { get; set; }
    }
}