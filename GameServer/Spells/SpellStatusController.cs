﻿using GameServer.Configuration;
using SharedData.Configuration;
using SharedData.Enums;
using System;

namespace GameServer.Spells
{
    public class SpellStatusController
    {
        private bool _isReady;
        private DateTime _totalReloadTime;

        public SpellStatusController(SpellName name)
        {
            _isReady = true;
            Name = name;
        }

        public SpellData Data
        {
            get
            {
                return ConfigurationManager.Instance.Data.Spells[Name];
            }
        }

        public SpellName Name
        {
            get;
            private set;
        }

        public bool IsReady
        {
            get
            {
                return _isReady;
            }
            set
            {
                _isReady = value;
                if (!_isReady)
                {
                    _totalReloadTime = DateTime.Now.AddMilliseconds(Data.ReloadTime);
                }
            }
        }

        public float CurrentReloadTime
        {
            get
            {
                float result = 0;
                if (!IsReady)
                {
                    result = (float)(_totalReloadTime - DateTime.Now).TotalMilliseconds;
                }
                return result;
            }
        }

        public float ReloadTimeInPercentages
        {
            get
            {
                float result = 0;
                if (!IsReady)
                {
                    result = CurrentReloadTime / Data.ReloadTime;
                }
                return result;
            }
        }

        public void UpdateSpell()
        {
            if (!IsReady)
            {
                if (DateTime.Now > _totalReloadTime)
                {
                    IsReady = true;
                }
            }
        }

        public void Reset()
        {
            IsReady = true;
        }
    }
}