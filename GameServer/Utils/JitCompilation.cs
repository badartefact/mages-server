﻿using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace GameServer.Utils
{
    public class JitCompilation
    {
        public static void ForceLoadAll(Assembly assembly)
        {
            ForceLoadAll(assembly, new HashSet<Assembly>());
        }

        public static void ForceLoadAll(Assembly assembly, HashSet<Assembly> loadedAssmblies)
        {
            var alreadyLoaded = !loadedAssmblies.Add(assembly);
            if (alreadyLoaded) return;

            PreJitMethods(assembly);

            var refrencedAssemblies = assembly.GetReferencedAssemblies();

            foreach (var curAssemblyName in refrencedAssemblies)
            {
                var nextAssembly = Assembly.Load(curAssemblyName);

                if (nextAssembly.GlobalAssemblyCache)
                {
                    continue;
                }

                ForceLoadAll(nextAssembly, loadedAssmblies);
            }
        }

        private static void PreJitMethods(Assembly assembly)
        {
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                var methods = type.GetMethods(
                        BindingFlags.DeclaredOnly |
                        BindingFlags.NonPublic |
                        BindingFlags.Public |
                        BindingFlags.Instance |
                        BindingFlags.Static);

                foreach (var methodInfo in methods)
                {
                    if (methodInfo.IsAbstract ||
                        methodInfo.ContainsGenericParameters)
                        continue;
                    try
                    {
                        RuntimeHelpers.PrepareMethod(methodInfo.MethodHandle);
                    }
                    catch { }
                }
            }
        }
    }
}