﻿using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class CodeAnswer : PacketBase
    {
        [ProtoMember(1)]
        public string Guid { get; set; }

        public CodeAnswer()
        {
            Code = Enums.PacketClassCode.CodeAnswer;
        }
    }
}