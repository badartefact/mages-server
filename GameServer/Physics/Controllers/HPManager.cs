﻿using System;

namespace GameServer.Physics.Controllers
{
    public class HpManager
    {
        private float _hitPoints = 100;

        public bool IsDead
        {
            get
            {
                return _hitPoints <= 0;
            }
        }

        public float HitPoints
        {
            get
            {
                return _hitPoints;
            }
            set
            {
                _hitPoints = value;
            }
        }

        public void AddHp(float amount)
        {
            _hitPoints += Math.Abs(amount);
        }

        public void RemoveHp(float amount)
        {
            _hitPoints -= Math.Abs(amount);
        }
    }
}