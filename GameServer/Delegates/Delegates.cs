﻿using SharedData.Enums;
using SharedData.Types;
using System;

namespace GameServer.Delegates
{
    public delegate void ConnectToGameHandler(object sender, Guid id);

    public delegate void DisconnectFromGameHandler(object sender, Guid id);

    public delegate void NewMessageHandler(object sender, byte[] data, int protoClass);

    public delegate void MoveHandler(object sender, TVector3 position);

    public delegate void CastSpellHandler(object sender, SpellName spellName, TVector3 target);
}