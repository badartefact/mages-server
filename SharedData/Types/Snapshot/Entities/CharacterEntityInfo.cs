﻿using SharedData.Enums;
using ProtoBuf;

namespace SharedData.Types.Snapshot.Entities
{
    [ProtoContract]
    public class CharacterEntityInfo : BaseEntity
    {
        [ProtoMember(1, IsRequired = true)]
        public bool IsConnected { get; set; }

        [ProtoMember(2, IsRequired = true)]
        public float HitPoints { get; set; }

        [ProtoMember(3, IsRequired = true)]
        public bool IsDead { get; set; }

        [ProtoMember(4, IsRequired = true)]
        public float Damage { get; set; }

        [ProtoMember(5, IsRequired = true)]
        public int CastingTime { get; set; }

        [ProtoMember(6, IsRequired = true)]
        public bool IsRunning { get; set; }

        [ProtoMember(7, IsRequired = true)]
        public bool IsCasting { get; set; }

        public CharacterEntityInfo()
        {
            Type = EntityType.Character;
        }
    }
}