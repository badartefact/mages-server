﻿using BEPUutilities;
using GameServer.Enums;
using GameServer.Physics;
using GameServer.Physics.Controllers;
using GameServer.Spells;
using SharedData.Enums;
using SharedData.Types;

namespace GameServer.Entities
{
    public class PlayerController : BaseEntityController
    {
        public TVector3 TPosition
        {
            get
            {
                return new TVector3
                {
                    X = CharacterEntityController.Position.X,
                    Y = CharacterEntityController.Position.Y,
                    Z = CharacterEntityController.Position.Z
                };
            }
        }

        public Vector3 Position
        {
            get
            {
                return CharacterEntityController.Position;
            }
        }

        public TQuaternion Rotation
        {
            get
            {
                return new TQuaternion
                {
                    X = CharacterEntityController.Rotation.X,
                    Y = CharacterEntityController.Rotation.Y,
                    Z = CharacterEntityController.Rotation.Z,
                    W = CharacterEntityController.Rotation.W
                };
            }
        }

        public string Guid { get; private set; }

        public string Name { get; private set; }

        public string Code { get; private set; }

        public PhysicsSpaceManager Space { get; set; }

        public CharacterEntityController CharacterEntityController { get; set; }

        public SpellManager SpellManagerController { get; set; }

        public CombatManager CombatManager { get; set; }

        public CharacterMovementController MovementController { get; set; }

        public float HitPoints
        {
            get
            {
                return CharacterEntityController.HitPoints;
            }
        }

        public bool IsDead
        {
            get { return CharacterEntityController.IsDead; }
        }

        public PlayerController(string code, string guid, string name)
            : base(ControllerEntityType.Character)
        {
            Code = code;
            Guid = guid;
            Name = name;

            MovementController = new CharacterMovementController(this);
            SpellManagerController = new SpellManager();
            CharacterEntityController = new CharacterEntityController(this);
            CombatManager = new CombatManager(this);
        }

        public void CreatePhysicalObject()
        {
            MovementController.CreatePhysicalObject();
        }

        public void Update()
        {
            CharacterEntityController.Update();
            MovementController.Update();
            SpellManagerController.Update();
        }

        public void MoveTo(Vector3 target)
        {
            CharacterEntityController.MoveTo(target);
        }

        public void CastSpell(SpellName spellName, Vector3 target)
        {
            CharacterEntityController.DoMagic(spellName, target);
        }

        public void Reset()
        {
            CharacterEntityController.Reset();
            MovementController.Reset();
            SpellManagerController.Reset();
        }

        public bool IsConnected { get; set; }
    }
}