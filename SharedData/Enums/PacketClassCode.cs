﻿namespace SharedData.Enums
{
    public enum PacketClassCode
    {
        AvailableGames,
        UpdateGame,
        UpdateSnapshot,
        RegisterAnswer,
        GetAvailableGames,
        ConnectToLobby,
        DisconnectFromLobbyRequest,
        SetReady,
        LoginInformation,
        CodeAnswer,
        CodeRecieverRequest,
        RegisterRequest,
        StartGameAnswer,
        MoveTo,
        CastSpell,
        DisconnectFromLobbyCommand,
        FinishData,
        GetMyPlayerConfigRequest,
        GetMyPlayerConfigAnswer
    }
}