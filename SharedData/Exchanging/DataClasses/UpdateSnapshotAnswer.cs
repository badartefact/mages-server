﻿using SharedData.Enums;
using SharedData.Types;
using SharedData.Types.Snapshot;
using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class UpdateSnapshotAnswer : PacketBase
    {
        [ProtoMember(1)]
        public SnapshotData Snapshot { get; set; }

        public UpdateSnapshotAnswer()
        {
            Code = PacketClassCode.UpdateSnapshot;
        }
    }
}