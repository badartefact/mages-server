﻿using SharedData.Enums;
using System;

namespace GameServer.Utils
{
    public static class SpellNameParser
    {
        public static SpellName Parse(string name)
        {
            var result = SpellName.None;
            foreach (SpellName spellName in Enum.GetValues(typeof(SpellName)))
            {
                if (spellName.ToString() == name)
                {
                    result = spellName;
                    break;
                }
            }
            return result;
        }
    }
}