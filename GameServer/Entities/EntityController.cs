﻿using GameServer.Enums;

namespace GameServer.Entities
{
    public class BaseEntityController
    {
        public ControllerEntityType EntityType { get; private set; }

        public BaseEntityController(ControllerEntityType entityType)
        {
            EntityType = entityType;
        }
    }
}