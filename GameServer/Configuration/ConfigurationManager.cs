﻿using Newtonsoft.Json;
using SharedData.Configuration;
using System;
using System.IO;

namespace GameServer.Configuration
{
    public class ConfigurationManager : FileWatcher
    {
        private const string PathToFile = "config.txt";
        private static ConfigurationManager _instance;

        public ConfigData Data { get; private set; }

        public static ConfigurationManager Instance
        {
            get { return _instance ?? (_instance = new ConfigurationManager()); }
        }

        private ConfigurationManager()
        {
            StartWatch(PathToFile);
            Initialize();
        }

        protected override sealed void Initialize()
        {
            try
            {
                var data = File.ReadAllText(PathToFile);
                Data = JsonConvert.DeserializeObject<ConfigData>(data);
                //File.WriteAllText(PathToFile, JsonConvert.SerializeObject(Data, Formatting.Indented));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cannot initialize config file: " + ex);
            }
        }
    }
}