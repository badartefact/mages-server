﻿using ProtoBuf;
using System;
using System.Collections.Generic;

namespace SharedData.Types
{
    [ProtoContract]
    public class ArenaGameData
    {
        [ProtoMember(1)]
        public Guid Id { get; set; }

        [ProtoMember(2)]
        public int MaximumAmount { get; set; }

        [ProtoMember(3)]
        public int CurrentAmount { get; set; }

        [ProtoMember(4)]
        public string Name { get; set; }

        [ProtoMember(5)]
        public string Comment { get; set; }

        [ProtoMember(6)]
        public List<PlayerGameData> Players { get; set; }
    }
}