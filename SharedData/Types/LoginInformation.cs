﻿using SharedData.Exchanging;
using ProtoBuf;

namespace SharedData.Types
{
    [ProtoContract]
    public class LoginInformation : PacketBase
    {
        [ProtoMember(1)]
        public string Name { get; set; }

        [ProtoMember(2)]
        public string Password { get; set; }

        public LoginInformation()
        {
            Code = Enums.PacketClassCode.LoginInformation;
        }
    }
}