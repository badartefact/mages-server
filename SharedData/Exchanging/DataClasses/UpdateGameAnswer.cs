﻿using SharedData.Enums;
using SharedData.Types;
using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class UpdateGameAnswer : PacketBase
    {
        [ProtoMember(1)]
        public ArenaGameData Game
        {
            get;
            set;
        }

        public UpdateGameAnswer()
        {
            Code = PacketClassCode.UpdateGame;
        }
    }
}