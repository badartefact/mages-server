﻿using SharedData.Enums;
using ProtoBuf;

namespace SharedData.Types.Snapshot.Entities
{
    [ProtoContract]
    [ProtoInclude(100, typeof(FireballEntity))]
    public class SpellEntity : BaseEntity
    {
        [ProtoMember(1, IsRequired = true)]
        public SpellName SpellName { get; set; }

        public SpellEntity()
        {
            Type = EntityType.Spell;
        }
    }
}