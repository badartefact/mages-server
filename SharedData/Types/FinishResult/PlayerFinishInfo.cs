﻿using ProtoBuf;

namespace SharedData.Types.FinishResult
{
    [ProtoContract]
    public class PlayerFinishInfo
    {
        [ProtoMember(2)]
        public string Name { get; set; }

        [ProtoMember(3)]
        public float TotalDamage { get; set; }
    }
}