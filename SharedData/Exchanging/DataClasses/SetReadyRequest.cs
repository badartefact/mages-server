﻿using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class SetReadyRequest : PacketBase
    {
        public SetReadyRequest()
        {
            Code = Enums.PacketClassCode.SetReady;
        }
    }
}