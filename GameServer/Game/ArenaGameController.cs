using BEPUutilities;
using GameServer.Configuration;
using GameServer.Entities;
using GameServer.Network;
using GameServer.Physics;
using GameServer.Physics.Controllers;
using GameServer.Spells.Controllers;
using GameServer.Statistics;
using GameServer.Utils;
using Lidgren.Network;
using SharedData.Enums;
using SharedData.Exchanging.DataClasses;
using SharedData.Logger;
using SharedData.Types;
using SharedData.Types.FinishResult;
using SharedData.Types.Snapshot;
using SharedData.Types.Snapshot.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace GameServer.Game
{
    public class ArenaGameController
    {
        #region Private members

        private List<PlayerController> _players;
        private PhysicsSpaceManager _physicsManager;
        private readonly SurfaceController _surfaceController;
        private ArenaNetworkClient _networkClient;
        private readonly ILogger _log;
        private readonly ArenaGameConfig _config;
        private readonly ArenaGameStatisticManager _statisticManager;
        private Timer _initTimer;

        #endregion Private members

        #region Public members

        public int CurrentRound
        {
            get;
            private set;
        }

        public int Port
        {
            get { return _config.Port; }
        }

        public event Action<ArenaGameController> OnAllDisconnected = delegate { };

        public event Action<ArenaGameController> OnGameEnded = delegate { };

        #endregion Public members

        #region Constructor

        public ArenaGameController(ILogger log, ArenaGameConfig config)
        {
            _config = config;
            _log = log;
            _players = new List<PlayerController>();
            _surfaceController = new SurfaceController();
            _statisticManager = new ArenaGameStatisticManager();

            InitTimer();

            CurrentRound = 1;
        }

        private void SubsribeToEvents()
        {
            //_initTimer.Elapsed += InitTimerElapsed;

            _networkClient.OnMoveTo += NetworkClientOnMoveTo;
            _networkClient.OnCastSpell += NetworkClientOnCastSpell;
            _networkClient.OnPlayerConnected += NetworkClientOnPlayerConnected;
            _networkClient.OnPlayerDisconnected += NetworkClientOnPlayerDisconnected;

            _physicsManager.OnCollision += PhysicsManagerOnCollision;
        }

        private void UnsubscribeFromEvents()
        {
            //_initTimer.Elapsed -= InitTimerElapsed;

            _networkClient.OnMoveTo -= NetworkClientOnMoveTo;
            _networkClient.OnCastSpell -= NetworkClientOnCastSpell;
            _networkClient.OnPlayerConnected -= NetworkClientOnPlayerConnected;
            _networkClient.OnPlayerDisconnected -= NetworkClientOnPlayerDisconnected;

            _physicsManager.OnCollision -= PhysicsManagerOnCollision;
        }

        private void InitTimer()
        {
            _initTimer = new Timer { Interval = 600000, AutoReset = false };
        }

        private void InitTimerElapsed(object sender, ElapsedEventArgs e)
        {
            //send to client special info with information that not all players connected
            UnsubscribeFromEvents();
            OnGameEnded(this);
        }

        #endregion Constructor

        #region Public methods

        public void Init(IEnumerable<Tuple<string, string, string>> players)
        {
            _players = players.Select(p => new PlayerController(p.Item1, p.Item2, p.Item3)).ToList();
            SetupNetwork();
            SetupPhysicsWorld();
            SubsribeToEvents();

            _initTimer.Start();
        }

        public void Update()
        {
            CheckRoundEnding();
            UpdatePhysics();
            SendSnapshot();
        }

        #endregion Public methods

        #region Player control

        private void CastSpell(PlayerController player, SpellName spellName, TVector3 target)
        {
            _log.Trace("Player '{0}' going to cast spell: '{1}'.", player.Name, spellName);
            player.CastSpell(spellName, new Vector3(target.X, target.Y, target.Z));
        }

        private void MoveTo(PlayerController player, TVector3 position)
        {
            player.MoveTo(new Vector3(position.X, position.Y, position.Z));
        }

        #endregion Player control

        #region Round control

        private void CheckLoadedPlayers()
        {
            var allPlayersAreReady = _players.All(p => p.IsConnected);
            if (allPlayersAreReady)
            {
                StartLevel();
            }
        }

        private void UpdatePhysics()
        {
            _physicsManager.Update();
            CheckSurface();
            foreach (var player in _players)
            {
                player.Update();
            }
        }

        private void CheckSurface()
        {
            var players = _players.Where(p => Vector3.Distance(p.Position, Vector3.Zero) > _surfaceController.Radius);
            foreach (var player in players)
            {
                player.CharacterEntityController.RemoveHp(ConfigurationManager.Instance.Data.Level.LavaDamage * Time.DeltaTime);
            }
        }

        private void CheckRoundEnding()
        {
            var alivePlayerCount = _players.Count(p => !p.IsDead);
            if (alivePlayerCount <= 1)
            {
                //for testing purposes
                if (_players.Count == 1 && !_players[0].IsDead) return;

                RunNextRound();
            }
        }

        private void RunNextRound()
        {
            if (CurrentRound + 1 > _config.MaxRounds)
            {
                SendFinishInfo();
                UnsubscribeFromEvents();
                OnGameEnded(this);
                return;
            }

            CurrentRound++;

            _players.ForEach(p => p.Reset());
            _surfaceController.Reset();
        }

        #endregion Round control

        #region Network

        private void SetupNetwork()
        {
            _networkClient = new ArenaNetworkClient(_log, _config.Port, _players.Select(p => new Tuple<string, string>(p.Code, p.Guid)).ToList());
        }

        private void SendStart()
        {
            var packet = new StartGameAnswer();
            _networkClient.SendToAll(packet);
        }

        private void SendSnapshot()
        {
            foreach (var playerController in _players)
            {
                var snapshot = GenerateSnapshotFor(playerController);
                var packet = new UpdateSnapshotAnswer { Snapshot = snapshot };
                _networkClient.Send(packet, new[] { playerController.Guid }, NetDeliveryMethod.UnreliableSequenced, 1);
            }
        }

        private void SendFinishInfo()
        {
            var finishData = GenerateFinishData();

            var packet = new FinishDataAnswer { FinishData = finishData };

            _networkClient.SendToAll(packet);
        }

        private void NetworkClientOnPlayerConnected(string playerId)
        {
            var player = GetPlayerById(playerId);
            if (player == null) return;

            player.IsConnected = true;

            CheckLoadedPlayers();
        }

        private void NetworkClientOnPlayerDisconnected(string playerId)
        {
            var player = GetPlayerById(playerId);
            if (player == null) return;

            player.IsConnected = false;

            CheckConnection();
        }

        private void CheckConnection()
        {
            var allDisconnected = _players.All(p => !p.IsConnected);
            if (allDisconnected)
            {
                OnAllDisconnected(this);
            }
        }

        private void NetworkClientOnCastSpell(string playerId, SpellName spellName, TVector3 position)
        {
            var player = GetPlayerById(playerId);
            if (player == null) return;

            CastSpell(player, spellName, position);
        }

        private void NetworkClientOnMoveTo(string playerId, TVector3 position)
        {
            var player = GetPlayerById(playerId);
            if (player == null) return;

            MoveTo(player, position);
        }

        public void Shutdown()
        {
            _networkClient.Shutdown();
        }

        #endregion Network

        #region Private methods

        private void StartLevel()
        {
            _statisticManager.AddPlayers(_players.Select(p => p.Guid));
            _surfaceController.Start();
            SendStart();
        }

        private void SetupPhysicsWorld()
        {
            _physicsManager = new PhysicsSpaceManager();

            AddPlayersToPhysicsWorld();
        }

        private void PhysicsManagerOnCollision(BaseEntityController source, BaseEntityController target)
        {
            CheckFireball(source, target);
        }

        #region Fireball collision

        private void CheckFireball(BaseEntityController source, BaseEntityController target)
        {
            var fireballController = source as FireballController;
            if (fireballController != null)
            {
                HandleFireballCollision(fireballController, target);
            }
        }

        private void HandleFireballCollision(FireballController fireballController, BaseEntityController target)
        {
            var anotherPlayer = target as PlayerController;
            if (anotherPlayer != null)
            {
                var impact = PhysicsHelper.CreateImpactObject(anotherPlayer.Position, fireballController.PhysicsObject.Position, fireballController.Force);
                anotherPlayer.CharacterEntityController.AddImpact(impact);
                if (anotherPlayer != fireballController.PlayerController)
                {
                    anotherPlayer.CharacterEntityController.RemoveHp(fireballController.Damage);
                    _statisticManager.DamageOccured(fireballController.PlayerController.Guid, anotherPlayer.Guid, fireballController.Damage);
                }
                fireballController.Destroy();
            }

            var anotherFireball = target as FireballController;
            if (anotherFireball != null)
            {
                fireballController.Destroy();
                anotherFireball.Destroy();
            }
        }

        #endregion Fireball collision

        private void AddPlayersToPhysicsWorld()
        {
            foreach (var player in _players)
            {
                player.Space = _physicsManager;
                player.CreatePhysicalObject();
            }
        }

        private PlayerController GetPlayerById(string id)
        {
            return _players.FirstOrDefault(p => p.Guid == id);
        }

        private FinishData GenerateFinishData()
        {
            var playersInfo = _players.ToDictionary(
                p => p.Guid,
                p => new PlayerFinishInfo { Name = p.Name, TotalDamage = _statisticManager[p.Guid].Damage });

            var player = GetWinner();
            _log.Debug("Winner: '{0}', with '{1}' amount of damage.", player.Name, _statisticManager[player.Guid].Damage);

            return new FinishData { WinnerId = player.Guid, PlayersInfo = playersInfo };
        }

        private PlayerController GetWinner()
        {
            var id = _statisticManager.GetPlayerWithHighestDamage();
            return _players.FirstOrDefault(p => p.Guid == id);
        }

        private SnapshotData GenerateSnapshotFor(PlayerController playerController)
        {
            var objectInScene = new List<BaseEntity>();
            foreach (var player in _players)
            {
                var playerInfo = new CharacterEntityInfo
                {
                    Id = player.Guid,
                    Position = player.TPosition,
                    Orientation = player.Rotation,
                    Name = player.Name,
                    IsConnected = player.IsConnected,
                    HitPoints = player.HitPoints,
                    IsDead = player.IsDead,
                    Damage = _statisticManager[player.Guid].Damage,
                    CastingTime = player.CharacterEntityController.CastingTime,
                    IsRunning = player.CharacterEntityController.IsRunning,
                    IsCasting = player.CharacterEntityController.IsCasting
                };

                objectInScene.Add(playerInfo);

                objectInScene.AddRange(_physicsManager.Spells.Select(spell => new SpellEntity
                {
                    Id = spell.Guid,
                    Position = spell.Position,
                    Orientation = spell.Rotation,
                    Name = spell.CustomName
                }));
            }

            return new SnapshotData
            {
                ObjectsOnScene = objectInScene,
                GameData =
                {
                    Radius = _surfaceController.Radius,
                    CurrentRound = CurrentRound,
                    MaximumRaunds = _config.MaxRounds,
                    SpellCooldowns = playerController.SpellManagerController.Spells.Where(s => s.Name != SpellName.None).ToDictionary(s => s.Name, s => s.ReloadTimeInPercentages)
                }
            };
        }

        #endregion Private methods
    }
}
