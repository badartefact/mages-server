﻿using SharedData.Exchanging;
using ProtoBuf;

namespace SharedData.Login
{
    [ProtoContract]
    public class CodeRecieverRequest : PacketBase
    {
        [ProtoMember(1)]
        public string Ip { get; set; }

        [ProtoMember(2)]
        public string Name { get; set; }

        public CodeRecieverRequest()
        {
            Code = Enums.PacketClassCode.CodeRecieverRequest;
        }
    }
}