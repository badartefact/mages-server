﻿using BEPUphysics;
using BEPUphysics.BroadPhaseEntries;
using BEPUphysics.BroadPhaseEntries.MobileCollidables;
using BEPUphysics.Entities;
using BEPUphysics.NarrowPhaseSystems.Pairs;
using BEPUutilities;
using GameServer.Entities;
using GameServer.Spells.Controllers;
using GameServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameServer.Physics
{
    public class PhysicsSpaceManager
    {
        public event Action<BaseEntityController, BaseEntityController> OnCollision = delegate { };

        private const float RTimeStep = 1f / 60f;
        private readonly Space _physicsSpace;

        public List<Entity> Entities
        {
            get { return _physicsSpace.Entities.ToList(); }
        }

        public PhysicsSpaceManager()
        {
            _physicsSpace = new Space();
            _physicsSpace.ForceUpdater.Gravity = Vector3.Zero;
            _physicsSpace.Solver.IterationLimit = 6;
            // _physicsSpace.BufferedStates.Enabled = true;
            //   _physicsSpace.Solver.IterationLimit = 6; //Don't need many iterations, there's not really any stacking going on in this game.
            _physicsSpace.TimeStepSettings.TimeStepDuration = RTimeStep;
        }

        #region Public methods

        public void Update()
        {
            _physicsSpace.Update();//Time.DeltaTime);

            foreach (var spell in Spells)
            {
                spell.Update();
            }
        }

        public IEnumerable<SpellController> Spells
        {
            get { return Entities.Where(e => e.Tag is SpellController).Select(e => e.Tag as SpellController); }
        }

        public IEnumerable<T> OverlapSphere<T>(Entity entity, float distance)
        {
            return Entities
                .Where(e => e.Tag is T
                            && Vector3.Distance(e.Position, entity.Position) < distance
                            && e != entity)
                .Select(e => (T)e.Tag);
        }

        public void Add(Entity entity)
        {
            entity.CollisionInformation.Events.InitialCollisionDetected += OnInitialCollisionDetected;
            _physicsSpace.Add(entity);
        }

        public void Remove(Entity entity)
        {
            if (_physicsSpace.Entities.Contains(entity))
            {
                entity.CollisionInformation.Events.InitialCollisionDetected -= OnInitialCollisionDetected;
                _physicsSpace.Remove(entity);
            }
        }

        #endregion Public methods

        private void OnInitialCollisionDetected(EntityCollidable sender, Collidable other, CollidablePairHandler pair)
        {
            //Console.WriteLine("Collision. Sender: '{0}'.", sender.Entity.Tag);

            var first = sender.Entity.Tag as BaseEntityController;
            var second = (other as EntityCollidable).Entity.Tag as BaseEntityController;

            if (first != null && second != null)
            {
                OnCollision(first, second);
            }
        }
    }
}