﻿using SharedData.Types.Snapshot.Entities;
using ProtoBuf;
using System.Collections.Generic;

namespace SharedData.Types.Snapshot
{
    [ProtoContract]
    public class SnapshotData
    {
        [ProtoMember(1)]
        public List<BaseEntity> ObjectsOnScene { get; set; }

        [ProtoMember(2)]
        public GameData GameData { get; set; }

        public SnapshotData()
        {
            GameData = new GameData();
        }
    }
}