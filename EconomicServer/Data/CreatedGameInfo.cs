﻿using EconomicServer.Lobby;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EconomicServer.Data
{
    public class CreatedGameInfo
    {
        public IEnumerable<Tuple<string, string, string>> PlayersInfo { get; private set; }

        public string Ip { get; private set; }

        public int Port { get; private set; }

        public CreatedGameInfo(IEnumerable<Tuple<string, string, string>> playersInfo, string ip, int port)
        {
            Port = port;
            Ip = ip;
            PlayersInfo = playersInfo;
        }
    }
}