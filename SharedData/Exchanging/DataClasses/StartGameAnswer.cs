﻿using SharedData.Enums;
using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class StartGameAnswer : PacketBase
    {
        [ProtoMember(1)]
        public string Ip { get; set; }

        [ProtoMember(2)]
        public int Port { get; set; }

        [ProtoMember(3)]
        public string GameCode { get; set; }

        public StartGameAnswer()
        {
            Code = PacketClassCode.StartGameAnswer;
        }
    }
}