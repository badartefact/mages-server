﻿using SharedData.Enums;
using SharedData.Types;
using ProtoBuf;
using System.Collections.Generic;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class ArenaGamesInfoAnswer : PacketBase
    {
        [ProtoMember(2)]
        public List<ArenaGameData> Games
        {
            get;
            set;
        }

        public ArenaGamesInfoAnswer()
        {
            Code = PacketClassCode.AvailableGames;
        }
    }
}