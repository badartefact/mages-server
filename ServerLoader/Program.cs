﻿using EconomicServer;
using System;

namespace ServerLoader
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var economicServer = new EconomicGod();
            economicServer.Start();

            Console.ReadLine();

            economicServer.Stop("On demand.");
        }
    }
}