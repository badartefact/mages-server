﻿namespace SharedData.Configuration
{
    public class Level
    {
        public float GroundStartRadius { get; set; }

        public float GroundUpdateInterval { get; set; }

        public float GroundUpdateValue { get; set; }

        public float LavaDamage { get; set; }
    }
}