﻿using ProtoBuf;

namespace SharedData.Types
{
    [ProtoContract]
    public class TQuaternion
    {
        [ProtoMember(1)]
        public float X { get; set; }

        [ProtoMember(2)]
        public float Y { get; set; }

        [ProtoMember(3)]
        public float Z { get; set; }

        [ProtoMember(4)]
        public float W { get; set; }
    }
}