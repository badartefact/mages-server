﻿namespace SharedData.Enums
{
    public enum SpellName
    {
        Fireball,
        Teleport,
        Push,
        TpCharge,
        Lighting,
        Explode,
        MagicChaser,
        None
    }
}