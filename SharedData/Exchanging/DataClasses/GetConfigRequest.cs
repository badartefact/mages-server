﻿using ProtoBuf;
using SharedData.Enums;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class GetConfigRequest : PacketBase
    {
        public GetConfigRequest()
        {
            Code = PacketClassCode.GetMyPlayerConfigRequest;
        }
    }
}