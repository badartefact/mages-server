﻿using SharedData.Configuration;
using SharedData.Enums;
using System;
using System.Linq;

namespace GameServer.Spells
{
    public class SpellManager
    {
        public SpellStatusController[] Spells
        {
            get
            {
                if (_spells == null)
                {
                    InitSpells();
                }
                return _spells;
            }
            set
            {
                _spells = value;
            }
        }

        private SpellStatusController[] _spells;

        private void InitSpells()
        {
            Spells = new SpellStatusController[7];
            Spells[0] = new SpellStatusController(SpellName.Fireball);
            Spells[1] = new SpellStatusController(SpellName.Explode);
            Spells[2] = new SpellStatusController(SpellName.Lighting);
            Spells[3] = new SpellStatusController(SpellName.None);
            Spells[4] = new SpellStatusController(SpellName.Teleport);
            Spells[5] = new SpellStatusController(SpellName.None);
            Spells[6] = new SpellStatusController(SpellName.None);
        }

        public bool IsExists(SpellName spellName)
        {
            var spell = Spells.Where(s => s.Name == spellName && s.Name != SpellName.None);

            return spell.Any();
        }

        public bool IsReady(SpellName spellName)
        {
            var result = false;
            var spell = Spells.FirstOrDefault(s => s.Name == spellName);
            if (spell != null)
            {
                result = spell.IsReady;
            }
            return result;
        }

        public void AddSpell(SpellName name, int position)
        {
            var controller = new SpellStatusController(name);
            Spells[position] = controller;
        }

        public SpellData GetSpellAt(int index)
        {
            if (index >= Spells.Count() || index < 0) return null;
            return Spells[index].Data;
        }

        public SpellData GetSpellByName(SpellName name)
        {
            return Spells.FirstOrDefault(s => s.Name == name).Data;
        }

        public void Update()
        {
            for (int i = 0; i < Spells.Count(); i++)
            {
                if (Spells[i].Name != SpellName.None)
                {
                    Spells[i].UpdateSpell();
                }
            }
        }

        public bool SpellNeedsTargeting(SpellName name)
        {
            var result = false;
            var spell = Spells.FirstOrDefault(s => s.Name == name);
            if (spell != null)
            {
                result = spell.Data.NeedTargeting;
            }
            else
            {
                Console.WriteLine("SpellNeedsTargeting: CANT FIND SKILL WITH NAME: " + name);
            }
            return result;
        }

        public void StartSpellCooldown(SpellName name)
        {
            var target = Spells.FirstOrDefault(s => s.Name == name);
            if (target != null)
            {
                target.IsReady = false;
            }
        }

        public SpellName[] GetAvailableSkills()
        {
            return Spells.Where(s => s.IsReady && s.Data.IsAttacking && s.Data.NeedTargeting).Select(s => s.Name).ToArray();
        }

        public void Reset()
        {
            Spells.ToList().ForEach(s => s.Reset());
        }
    }
}