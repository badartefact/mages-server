﻿using SharedData.Enums;

namespace SharedData.Exchanging
{
    public class PacketBase
    {
        public PacketClassCode Code { get; set; }
    }
}