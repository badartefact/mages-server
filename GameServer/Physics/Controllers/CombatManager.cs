﻿namespace GameServer.Physics.Controllers
{
    using BEPUutilities;
    using GameServer.Entities;
    using GameServer.Spells;
    using GameServer.Spells.Controllers;
    using GameServer.Utils;
    using SharedData.Configuration;
    using SharedData.Enums;
    using System;

    public class CombatManager
    {
        private readonly PlayerController _playerController;

        public CombatManager(PlayerController controller)
        {
            _playerController = controller;
        }

        public bool SpellNeedRotation(SpellName spell)
        {
            switch (spell)
            {
                case SpellName.Teleport:
                    return false;

                case SpellName.Push:
                    return false;

                case SpellName.Explode:
                    return false;

                default:
                    return true;
            }
        }

        public void ThrowFireball()
        {
            var from = _playerController.MovementController.Position;
            var to = _playerController.MovementController.Target;

            var fireballData = _playerController.SpellManagerController.GetSpellByName(SpellName.Fireball);
            var fireball = new FireballController(_playerController);
            fireball.Send(from, to, fireballData.Speed, fireballData.Damage, fireballData.Distance, fireballData.Force, fireballData.ExplodeDistance);
        }

        public void Teleport()
        {
            var from = _playerController.MovementController.Position;
            var to = _playerController.MovementController.Target;
            var result = to;

            var teleportData = _playerController.SpellManagerController.GetSpellByName(SpellName.Teleport);
            if (Vector3.Distance(from, to) > teleportData.Distance)
            {
                var forward = to - from;
                forward.Normalize();
                var withDistance = forward * teleportData.Distance;
                result = from + new Vector3(withDistance.X, withDistance.Y, withDistance.Z);
                result = new Vector3(result.X, from.Y, result.Z);
            }

            _playerController.MovementController.PhysicsObject.LookAt(result);

            _playerController.MovementController.Target = result;
            _playerController.MovementController.Position = result;
        }

        public void Push()
        {
            var direction = _playerController.MovementController.Target - _playerController.MovementController.Position;
            direction.Normalize();

            //_playerController.MovementController.PushMe(direction);
        }

        public void TpCharge()
        {
            var from = _playerController.MovementController.Position;
            var to = _playerController.MovementController.Target;

            var result = to;

            var tpChargeData = _playerController.SpellManagerController.GetSpellByName(SpellName.TpCharge);

            if (Vector3.Distance(from, to) > tpChargeData.Distance)
            {
                var forward = _playerController.MovementController.PhysicsObject.OrientationMatrix.Forward;// GetComponent<CharacterMovementController>().transform.forward;
                var withDistance = forward * tpChargeData.Distance;
                result = from + withDistance;
                result = new Vector3(result.X, from.Y, result.Z);
            }

            /*
            GameObject tpCharge = null;
            tpCharge = (GameObject)Instantiate(Resources.Load("TpCharge"), from, Quaternion.identity);
            tpCharge.GetComponent<TpChargeController>().Send(result, tpChargeData.Speed, gameObject);*/
        }

        public void ThrowLighting()
        {/*
        Vector3 from = _playerController.MovementController.Position;
        Vector3 to = _playerController.MovementController.Target;

        Vector3 direction = to - from;
        direction.Normalize();

        float distance = Vector3.Distance(from, to);

        SpellBase lightingData = _playerController.SpellManager.GetSpellByName(SpellName.Lighting);

        float lightingThrowDistance = lightingData.Distance;
        if (distance > lightingThrowDistance)
        {
            distance = lightingThrowDistance;
            Vector3 newTo = direction * lightingThrowDistance;
            to = newTo + from;
        }

        Ray checker = new Ray(from, direction);
        RaycastHit hitInfo;
        if (Physics.Raycast(checker, out hitInfo, distance))
        {
            if (hitInfo.transform.tag == "fighter")
            {
                ImpactObject impact = new ImpactObject(direction, lightingData.Force);
                hitInfo.transform.gameObject.SendMessage("AddDamage", lightingData.Damage);
                hitInfo.transform.gameObject.SendMessage("AddImpact", impact);
                to = hitInfo.point;
            }
            if (hitInfo.transform.tag == "fireball")
            {
                hitInfo.transform.SendMessage("Explode");
                to = hitInfo.point;
            }
        }
        GameObject lighting = (GameObject)Instantiate(Resources.Load("Lighting"), from, Quaternion.identity);
        lighting.GetComponent<LightingController>().Send(from, to);*/
        }

        public void Explode()
        {
            SpellData explodeData = _playerController.SpellManagerController.GetSpellByName(SpellName.Explode);
            _playerController.CharacterEntityController.RemoveHp(explodeData.SelfDamage);
            var players = _playerController.Space.OverlapSphere<PlayerController>(_playerController.MovementController.PhysicsObject, explodeData.ExplodeDistance);
            foreach (var player in players)
            {
                var direction = (player.Position - _playerController.MovementController.PhysicsObject.Position);
                direction.Normalize();
                var impact = new ImpactObject(direction, explodeData.Force);
                player.CharacterEntityController.RemoveHp(explodeData.Damage);
                player.CharacterEntityController.AddImpact(impact);
            }
        }

        public void ThrowMagicChaser()
        {
            /*
        Vector3 from = _playerController.MovementController.Position;
        Vector3 to = _playerController.MovementController.Target;

        GameObject magicChaser = null;
        magicChaser = (GameObject)Instantiate(Resources.Load("MagicChaser"), from, Quaternion.identity);
        SpellBase magicChaserData = _playerController.SpellManager.GetSpellByName(SpellName.MagicChaser);
        magicChaser.GetComponent<MagicChaserController>().Send(to, magicChaserData.Speed, magicChaserData.Damage, magicChaserData.Force, magicChaserData.Duration, gameObject);
   */
        }
    }
}