﻿using BEPUphysics.Entities;
using BEPUutilities;

namespace GameServer.Utils
{
    public static class PhysicsHelper
    {
        public static ImpactObject CreateImpactObject(Vector3 from, Vector3 to, float force)
        {
            var direction = from - to;
            direction.Normalize();
            return new ImpactObject(direction, force);
        }

        public static void LookAt(this Entity entity, Vector3 target)
        {
            var currentOrientation = entity.OrientationMatrix.Forward;
            var difference = target - entity.Position;
            difference.Normalize();

            Quaternion result;
            Quaternion.GetQuaternionBetweenNormalizedVectors(ref currentOrientation, ref difference, out result);

            result = Quaternion.Concatenate(entity.Orientation, result);

            entity.Orientation = result;
        }
    }
}