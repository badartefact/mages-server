﻿using System;

namespace SharedData.Exchanging
{
    public class PacketDataManager
    {
        #region Private members

        private byte[] _receivingBuffer;
        private byte[] _sendingBuffer;
        private object _sync = new object();

        private const uint RPolynomial = 0xEDB88320;
        private const int RHeaderSize = 6;
        private const int RcrcSize = 4;

        #endregion Private members

        #region Public members

        public delegate void NewDataHandler(object sender, byte[] data, int protoClass);

        public delegate void NewDataFromSocketHandler(object sender, byte[] data, int protoClass, object socket);

        public event NewDataHandler OnNewData = delegate { };

        public event EventHandler OnError = delegate { };

        public event NewDataFromSocketHandler OnNewDataFromSocket = delegate { };

        #endregion Public members

        #region Public methods

        public void Parse(byte[] buffer, object client = null)
        {
            try
            {
                _receivingBuffer = buffer;

                UInt16 packetSize = GetInfoFromHeader(0);
                UInt16 dataSize = GetInfoFromHeader(2);
                UInt16 packetClass = GetInfoFromHeader(4);

                var calculatedCrc = CalculateCRC(buffer, packetSize - RcrcSize);
                var receivedCrc = GetCrcFromPacket(packetSize - RcrcSize);

                if (calculatedCrc == receivedCrc)
                {
                    var data = new byte[dataSize];
                    Array.Copy(buffer, RHeaderSize, data, 0, dataSize);
                    if (client == null)
                    {
                        OnNewData(this, data, packetClass);
                    }
                    else
                    {
                        OnNewDataFromSocket(this, data, packetClass, client);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                OnError(this, new EventArgs());
            }
        }

        public byte[] GetBytes(PacketBase packet)
        {
            lock (_sync)
            {
                byte[] data = DataSerializer<PacketBase>.GetBytes(packet);
                int packetSize = RHeaderSize + data.Length + RcrcSize;

                _sendingBuffer = new byte[packetSize];

                SetPacketSize(packetSize);
                SetDataSize(data.Length);
                SetProtoClass((int)packet.Code);
                SetData(data);
                SetCrc(packetSize - RcrcSize);
            }
            return _sendingBuffer;
        }

        #endregion Public methods

        #region Private methods

        private void SetData(byte[] data)
        {
            Array.Copy(data, 0, _sendingBuffer, RHeaderSize, data.Length);
        }

        private void SetProtoClass(int value)
        {
            var bytesData = BitConverter.GetBytes(value);
            Array.Reverse(bytesData);
            _sendingBuffer[4] = bytesData[2];
            _sendingBuffer[5] = bytesData[3];
        }

        private void SetDataSize(int value)
        {
            var bytesData = BitConverter.GetBytes(value);
            Array.Reverse(bytesData);
            _sendingBuffer[2] = bytesData[2];
            _sendingBuffer[3] = bytesData[3];
        }

        private void SetPacketSize(int value)
        {
            var bytesData = BitConverter.GetBytes(value);
            Array.Reverse(bytesData);
            _sendingBuffer[0] = bytesData[2];
            _sendingBuffer[1] = bytesData[3];
        }

        private void SetCrc(int size)
        {
            var crc = CalculateCRC(_sendingBuffer, size);
            var crcBytes = BitConverter.GetBytes(crc);
            Array.Copy(crcBytes, 0, _sendingBuffer, size, RcrcSize);
        }

        private uint CalculateCRC(byte[] data, int length)
        {
            var result = 0xFFFFFFFF;

            var tableCrc32 = new uint[256];

            unchecked
            {
                for (int i = 0; i < 256; i++)
                {
                    var crc32 = (uint)i;

                    for (int j = 8; j > 0; j--)
                    {
                        if ((crc32 & 1) == 1)
                            crc32 = (crc32 >> 1) ^ RPolynomial;
                        else
                            crc32 >>= 1;
                    }

                    tableCrc32[i] = crc32;
                }

                for (int i = 0; i < length; i++)
                {
                    result = ((result) >> 8)
                        ^ tableCrc32[(data[i])
                        ^ ((result) & 0x000000FF)];
                }
            }

            return ~result;
        }

        private uint GetCrcFromPacket(int startIndex)
        {
            var dataBytes = new byte[RcrcSize];
            Array.Copy(_receivingBuffer, startIndex, dataBytes, 0, RcrcSize);
            return BitConverter.ToUInt32(dataBytes, 0);
        }

        private UInt16 GetInfoFromHeader(int startIndex)
        {
            var dataBytes = new byte[2];
            Array.Copy(_receivingBuffer, startIndex, dataBytes, 0, 2);
            Array.Reverse(dataBytes);
            return BitConverter.ToUInt16(dataBytes, 0);
        }

        public static UInt16 GetPacketLength(byte[] buffer)
        {
            var dataBytes = new byte[2];
            Array.Copy(buffer, 0, dataBytes, 0, 2);
            Array.Reverse(dataBytes);
            return BitConverter.ToUInt16(dataBytes, 0);
        }

        #endregion Private methods
    }
}