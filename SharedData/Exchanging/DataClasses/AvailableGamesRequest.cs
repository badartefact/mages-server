﻿using SharedData.Enums;
using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class AvailableGamesRequest : PacketBase
    {
        public AvailableGamesRequest()
        {
            Code = PacketClassCode.GetAvailableGames;
        }
    }
}