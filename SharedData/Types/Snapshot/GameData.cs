﻿using SharedData.Enums;
using ProtoBuf;
using System.Collections.Generic;

namespace SharedData.Types.Snapshot
{
    [ProtoContract]
    public class GameData
    {
        [ProtoMember(1)]
        public float Radius { get; set; }

        [ProtoMember(2)]
        public int CurrentRound { get; set; }

        [ProtoMember(3)]
        public int MaximumRaunds { get; set; }

        [ProtoMember(4)]
        public Dictionary<SpellName, float> SpellCooldowns { get; set; }
    }
}