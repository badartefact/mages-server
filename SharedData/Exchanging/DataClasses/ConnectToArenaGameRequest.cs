﻿using SharedData.Enums;
using ProtoBuf;
using System;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class ConnectToArenaGameRequest : PacketBase
    {
        [ProtoMember(1)]
        public Guid Id
        {
            get;
            set;
        }

        public ConnectToArenaGameRequest()
        {
            Code = PacketClassCode.ConnectToLobby;
        }
    }
}