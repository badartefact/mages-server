﻿namespace GameServer.Statistics
{
    public class ArenaPlayerStatistic
    {
        public static ArenaPlayerStatistic Empty = new ArenaPlayerStatistic() { Damage = 0 };

        public float Damage { get; set; }
    }
}