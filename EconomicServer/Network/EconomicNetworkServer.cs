﻿using Lidgren.Network;
using SharedData;
using SharedData.Enums;
using SharedData.Exchanging;
using SharedData.Exchanging.DataClasses;
using SharedData.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace EconomicServer.Network
{
    public class EconomicNetworkServer
    {
        #region Private members

        private readonly object _sync = new object();
        private readonly string _configName;
        private readonly int _port;
        private readonly ILogger _log;

        private NetServer _server;
        private PacketDataManager _packetManager;
        private readonly Dictionary<string, NetConnection> _connectedPlayers;

        #endregion Private members

        #region Public members

        public event Action<string> OnAvailableGamesRequest = delegate { };

        public event Action<string, Guid> OnConnectToLobby = delegate { };

        public event Action<string> OnDisconnectFromLobby = delegate { };

        public event Action<string, string> OnPlayerConnectedToServer = delegate { };

        public event Action<string> OnPlayerDisconnectedFromServer = delegate { };

        public event Action<string> OnSetReady = delegate { };

        #endregion Public members

        #region Constructor

        public EconomicNetworkServer(string configName, int port, ILogger log)
        {
            _log = log;
            _configName = configName;
            _port = port;

            _connectedPlayers = new Dictionary<string, NetConnection>();

            InitPacketManager();
            InitNetworkClient();
        }

        private void InitNetworkClient()
        {
            var config = new NetPeerConfiguration(_configName) { Port = _port };
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            _server = new NetServer(config);
            var context = new SynchronizationContext();
            _server.RegisterReceivedCallback(GotMessage, context);
        }

        #endregion Constructor

        #region Public methods

        public void SendToAll(PacketBase packet)
        {
            _log.Debug("Going to send: " + packet.Code);
            var msg = _server.CreateMessage();
            byte[] result = _packetManager.GetBytes(packet);
            msg.Write(result);

            foreach (var connectedPlayer in _connectedPlayers)
            {
                Send(connectedPlayer.Key, packet);
            }

            // looks like SendToAll method is not sending packet sometimes
            //_server.SendToAll(msg, NetDeliveryMethod.ReliableOrdered);
        }

        public void Send(string id, PacketBase packet, int channel = 0)
        {
            var connection = GetConnectionById(id);
            if (connection == null)
            {
                _log.Trace("Was about to send '{0}', but connection is == null, removing it.", packet.Code);
                _connectedPlayers.Remove(id);
                return;
            }

            _log.Trace("Going to send packet class: " + packet.Code);
            var msg = _server.CreateMessage();
            var result = _packetManager.GetBytes(packet);
            msg.Write(result);
            var sentResult = _server.SendMessage(msg, connection, NetDeliveryMethod.ReliableOrdered, channel);
            _log.Trace("Send result: " + sentResult);
            if (sentResult != NetSendResult.Sent)
            {
                _log.Warn("Cannot send message of protoClass: {0}. Result: {1}", packet.Code, sentResult);
            }
        }

        private NetConnection GetConnectionById(string id)
        {
            NetConnection result = null;
            if (_connectedPlayers.ContainsKey(id))
            {
                result = _connectedPlayers[id];
            }
            return result;
        }

        public void Start()
        {
            _server.Start();
            SubscribeToEvents();

            _log.Debug("EconomicNetworkServer started.");
        }

        public void Stop(string message)
        {
            _server.Shutdown(message);
            UnSubscribeFromEvents();

            _log.Debug("EconomicNetworkServer stopped.");
        }

        #endregion Public methods

        #region Events

        private void SubscribeToEvents()
        {
            _packetManager.OnNewDataFromSocket += OnNewDataFromSocket;
        }

        private void UnSubscribeFromEvents()
        {
            _packetManager.OnNewDataFromSocket -= OnNewDataFromSocket;
        }

        private void OnNewDataFromSocket(object sender, byte[] data, int protoClass, object status)
        {
            var connection = status as NetConnection;

            lock (_sync)
            {
                _log.Trace("Received protoClass from packetManager: {0}", protoClass);
                var request = (PacketClassCode)protoClass;

                var playerId = GetPlayerIdByConnection(connection);
                if (playerId == null) return;

                switch (request)
                {
                    case PacketClassCode.GetAvailableGames:

                        OnAvailableGamesRequest(playerId);
                        break;

                    case PacketClassCode.ConnectToLobby:
                        var connectRequest = DataSerializer<ConnectToArenaGameRequest>.Get(data);

                        OnConnectToLobby(playerId, connectRequest.Id);
                        break;

                    case PacketClassCode.DisconnectFromLobbyRequest:
                        OnDisconnectFromLobby(playerId);
                        break;

                    case PacketClassCode.SetReady:
                        OnSetReady(playerId);
                        break;

                    default:
                        _log.Debug("Received unhandled protoClass: {0}", protoClass);
                        break;
                }
            }
        }

        #endregion Events

        #region Private methods

        private string GetPlayerIdByConnection(NetConnection connection)
        {
            return _connectedPlayers.Where(p => p.Value == connection).Select(p => p.Key).FirstOrDefault();
        }

        private void InitPacketManager()
        {
            _packetManager = new PacketDataManager();
        }

        private void GotMessage(object state)
        {
            lock (_sync)
            {
                var message = _server.ReadMessage();

                _log.Trace("Received message from server: {0}", message.MessageType);

                switch (message.MessageType)
                {
                    case NetIncomingMessageType.Data:

                        var buffer = new byte[1024];

                        message.ReadBytes(buffer, 0, 2);
                        var packetSize = PacketDataManager.GetPacketLength(buffer);
                        message.ReadBytes(buffer, 2, packetSize - 2);
                        _packetManager.Parse(buffer, message.SenderConnection);
                        break;

                    case NetIncomingMessageType.ConnectionApproval:
                        var name = message.ReadString();
                        _log.Debug("Got approval message: {0}", name);
                        message.SenderConnection.Approve();

                        var id = Guid.NewGuid().ToString();

                        _connectedPlayers.Add(id, message.SenderConnection);
                        Thread.Sleep(100);
                        OnPlayerConnectedToServer(id, name);
                        break;

                    case NetIncomingMessageType.StatusChanged:
                        var status = message.SenderConnection.Status;
                        if (status == NetConnectionStatus.Disconnected)
                        {
                            var playerId = GetPlayerIdByConnection(message.SenderConnection);
                            if (playerId == null) return;

                            _connectedPlayers.Remove(playerId);
                            OnPlayerDisconnectedFromServer(playerId);
                        }
                        break;

                    default:
                        _log.Debug("Received unhandled NetIncomingMessageType. Type: '{0}', Data: '{1}'", message.MessageType, message.PeekString());
                        break;
                }

                _server.Recycle(message);
            }
        }

        #endregion Private methods
    }
}