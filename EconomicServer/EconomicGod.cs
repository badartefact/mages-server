﻿using EconomicServer.Data;
using EconomicServer.Lobby;
using EconomicServer.Logger;
using EconomicServer.Network;
using Lidgren.Network;
using SharedData.Exchanging.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace EconomicServer
{
    public class EconomicGod
    {
        #region Constants

        private const string ConfigName = "MAGES";
        private const int Port = 34000;

        #endregion Constants

        #region Private members

        private readonly Dictionary<string, PlayerController> _players;
        private readonly List<GameController> _lobbyGames;
        private int _gameIndex = 0;
        private EconomicNetworkServer _server;
        private readonly ArenaGameCreator _arenaGameCreator;
        private readonly NLogger _log;

        #endregion Private members

        #region Constructor

        public EconomicGod()
        {
            _log = new NLogger("EconomicGod");
            _players = new Dictionary<string, PlayerController>();
            _lobbyGames = new List<GameController>();
            _arenaGameCreator = new ArenaGameCreator(_log);

            InitServer();
            CreateStandartGame();
        }

        #endregion Constructor

        #region Public methods

        public void Start()
        {
            _server.Start();
            SubscribeToEvents();
            _log.Info("Economic God arrives!");
        }

        public void Stop(string message)
        {
            UnsubscribeFromEvents();
            _server.Stop(message);
        }

        #endregion Public methods

        #region Events

        private void SubscribeToEvents()
        {
            _server.OnPlayerConnectedToServer += OnPlayerConnectedToServer;
            _server.OnAvailableGamesRequest += OnAvailableGamesRequest;
            _server.OnConnectToLobby += OnConnectToLobby;
            _server.OnDisconnectFromLobby += OnDisconnectFromLobby;
            _server.OnSetReady += OnSetReady;
            _server.OnPlayerDisconnectedFromServer += OnPlayerDisconnectedFromServer;
        }

        private void UnsubscribeFromEvents()
        {
            _server.OnPlayerConnectedToServer -= OnPlayerConnectedToServer;
            _server.OnAvailableGamesRequest -= OnAvailableGamesRequest;
            _server.OnConnectToLobby -= OnConnectToLobby;
            _server.OnDisconnectFromLobby -= OnDisconnectFromLobby;
            _server.OnSetReady -= OnSetReady;
            _server.OnPlayerDisconnectedFromServer -= OnPlayerDisconnectedFromServer;
        }

        private void OnPlayerDisconnectedFromServer(string id)
        {
            if (_players.ContainsKey(id))
            {
                _log.Info("Player '{0}' disconnected from economic server.", _players[id].Name);
                _players.Remove(id);
                RemoveFromLobby(id);
            }
        }

        private void OnSetReady(string id)
        {
            if (!_players.ContainsKey(id))
            {
                _log.Warn("Got ready command, but cannot find from whom.");
                return;
            }

            var player = _players[id];

            player.IsReadyToStartGame = true;

            _log.Info("'{0}' is ready to start the game.", player.Name);

            var game = _lobbyGames.FirstOrDefault(lobbyGame => lobbyGame.Players.Contains(player));
            SendUpdateGame(game);

            CheckAllPlayersIfTheyAreReadyToStartTheGame();

            SendAvailableGames();
        }

        private void OnDisconnectFromLobby(string id)
        {
            if (!_players.ContainsKey(id))
            {
                _log.Warn("Player with id: '{0}' requested OnDisconnectFromLobby, but player cannot be found.");
                return;
            }
            var player = _players[id];

            var game = _lobbyGames.FirstOrDefault(lobbyGame => lobbyGame.Players.Contains(player));
            if (game == null)
            {
                _log.Warn("Player with id: '{0}' requested OnDisconnectFromLobby, but he is not connected to any lobby.");
                return;
            }

            player.IsConnectedToLobby = false;

            ResetPlayersReadyState(game);
            game.RemovePlayerFromLobby(player);

            _log.Info("'{0}' disconnected from the lobby.", player.Name);

            SendAvailableGames();
            SendUpdateGame(game);
        }

        private void ResetPlayersReadyState(GameController game)
        {
            game.Players.ForEach(p => p.IsReadyToStartGame = false);
        }

        private void OnConnectToLobby(string id, Guid guid)
        {
            var game = _lobbyGames.FirstOrDefault(lobbyGame => lobbyGame.Id == guid);
            if (game == null) return;

            if (!_players.ContainsKey(id)) return;
            var player = _players[id];

            if (!game.Players.Contains(player))
            {
                game.AddPlayer(player);
                player.IsConnectedToLobby = true;
                _log.Info("'{0}' connected to the lobby.", player.Name);
            }

            SendAvailableGames();
            SendUpdateGame(game);
        }

        private void OnAvailableGamesRequest(string id)
        {
            var availableGames = new ArenaGamesInfoAnswer
            {
                Games = _lobbyGames.Select(game => game.GetData()).ToList()
            };
            _server.Send(id, availableGames);

            _log.Debug("Sending available games.");
        }

        private void OnPlayerConnectedToServer(string guid, string name)
        {
            var player = new PlayerController(guid, name);

            _players.Add(guid, player);

            _log.Debug("PLAY SERVER: Player connected: " + name);
            Thread.Sleep(50);

            SendAccept(player.Id);
        }

        #endregion Events

        #region Private methods

        private void InitServer()
        {
            _server = new EconomicNetworkServer(ConfigName, Port, _log);
        }

        private void SendUpdateGame(GameController game)
        {
            var gameUpdate = new UpdateGameAnswer { Game = game.GetData() };
            foreach (var connectedPlayer in game.Players)
            {
                _server.Send(connectedPlayer.Id, gameUpdate);
            }
        }

        private void SendAvailableGames()
        {
            var availableGames = new ArenaGamesInfoAnswer
            {
                Games = _lobbyGames.Select(game => game.GetData()).ToList()
            };
            _server.SendToAll(availableGames);
        }

        private void SendAccept(string id)
        {
            var answer = new RegisterAnswer { CodeAccepted = true };
            _server.Send(id, answer);
        }

        private void CreateStandartGame()
        {
            CreateGame("Game #" + _gameIndex, "Comment to game#" + _gameIndex, 12);
            _gameIndex++;
        }

        private void CreateGame(string name, string comment, int maximumAmount)
        {
            var game = new GameController
            {
                Comment = comment,
                MaximumAmount = maximumAmount,
                Name = name,
                Id = Guid.NewGuid()
            };

            _lobbyGames.Add(game);
        }

        private void CheckAllPlayersIfTheyAreReadyToStartTheGame()
        {
            var games = new List<GameController>(_lobbyGames.Where(game => game.Players.All(p => p.IsReadyToStartGame)));
            foreach (var game in games)
            {
                ResetPlayersReadyState(game);
                StartGame(game);
                _lobbyGames.Remove(game);
                CreateStandartGame();
            }
        }

        private void StartGame(GameController game)
        {
            var playerCodes = _arenaGameCreator.CreateArenaGame(game.Players);

            if (playerCodes != null)
            {
                foreach (var tuple in playerCodes.PlayersInfo)
                {
                    _log.Debug("Code: {0}, Name: {1}, Id: {2}", tuple.Item1, tuple.Item3, tuple.Item2);
                }
                SendCreatedGameInfoToPlayers(playerCodes);
                _log.Debug("Game has been started.");
            }
            else
            {
                _log.Warn("Some error occurred. Resetting to default state.");
                _lobbyGames.Remove(game);
                game.Players.ForEach(p => p.IsConnectedToLobby = false);

                SendAvailableGames();
            }
        }

        private void SendCreatedGameInfoToPlayers(CreatedGameInfo playerCodes)
        {
            var ip = playerCodes.Ip;
            var port = playerCodes.Port;

            foreach (var playerInfo in playerCodes.PlayersInfo)
            {
                var player = _players.Values.FirstOrDefault(p => p.Id == playerInfo.Item2);
                var code = playerInfo.Item1;

                var packet = new StartGameAnswer { GameCode = code, Ip = ip, Port = port };
                _server.Send(player.Id, packet);
            }
        }

        private void RemoveFromLobby(string id, bool sendNotificationToDisconnectedPlayer = false)
        {
            _log.Trace("Going to try to remove from lobby player with id: '{0}'.", id);
            var game = _lobbyGames.FirstOrDefault(g => g.Players.FirstOrDefault(p => p.Id == id) != null);
            if (game != null)
            {
                _log.Debug("Player with id '{0}' was removed from lobby.", id);
                game.RemovePlayerFromLobby(id);

                if (sendNotificationToDisconnectedPlayer) SendDisconnectedFromLobby(id);
            }
        }

        private void SendDisconnectedFromLobby(string id)
        {
            var command = new DisconnectFromLobbyCommand();
            _server.Send(id, command);
        }

        #endregion Private methods
    }
}