﻿using System;

namespace GameServer.Utils
{
    internal class TrueRandom
    {
        private static readonly Random RandomEngine = new Random();
        private static readonly object SyncLock = new object();

        public static int GetRandomNumber(int min, int max)
        {
            lock (SyncLock)
            {
                return RandomEngine.Next(min, max);
            }
        }
    }
}