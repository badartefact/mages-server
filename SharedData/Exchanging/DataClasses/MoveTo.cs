﻿using SharedData.Types;
using ProtoBuf;

namespace SharedData.Exchanging.DataClasses
{
    [ProtoContract]
    public class MoveTo : PacketBase
    {
        [ProtoMember(1)]
        public TVector3 Position { get; set; }

        public MoveTo()
        {
            Code = Enums.PacketClassCode.MoveTo;
        }
    }
}