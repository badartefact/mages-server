﻿using BEPUphysics.Entities;
using BEPUphysics.Entities.Prefabs;
using BEPUutilities;
using GameServer.Configuration;
using GameServer.Entities;
using GameServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameServer.Physics.Controllers
{
    public class CharacterMovementController
    {
        private float MovementSpeed
        {
            get
            {
                return ConfigurationManager.Instance.Data.Physics.MovementSpeed;
            }
        }

        private float RotationSpeed
        {
            get
            {
                return ConfigurationManager.Instance.Data.Physics.RotationSpeed;
            }
        }

        private Vector3 _target;

        private readonly List<ImpactObject> _impacts = new List<ImpactObject>();

        private readonly PlayerController _playerController;

        public Entity PhysicsObject
        {
            get;
            set;
        }

        public Vector3 Target
        {
            get
            {
                return _target;
            }
            set
            {
                _target = new Vector3(value.X, 1, value.Z);
            }
        }

        public Vector3 Position
        {
            get
            {
                return PhysicsObject.Position;
            }
            set
            {
                PhysicsObject.Position = value;
            }
        }

        public Vector3 Velocity
        {
            get
            {
                return PhysicsObject.LinearVelocity;
            }
            set
            {
                PhysicsObject.LinearVelocity = value;
            }
        }

        public Quaternion Rotation
        {
            get
            {
                return PhysicsObject.Orientation;
            }
            set
            {
                PhysicsObject.Orientation = value;
            }
        }

        public CharacterMovementController(PlayerController playerController)
        {
            _impacts = new List<ImpactObject>();
            _playerController = playerController;
        }

        public void AddImpact(ImpactObject impact)
        {
            _impacts.Add(impact);
        }

        private void ComsumeImpacts()
        {
            for (int i = 0; i < _impacts.Count; i++)
            {
                var impact = _impacts[i];
                impact.ConsumeImpact();
                if (impact.Dead)
                {
                    _impacts.RemoveAt(i);
                }
            }
        }

        public void Update()
        {
            var impact = GetnerateCommonImpact();
            Velocity = Velocity + impact * Time.DeltaTime;
            ComsumeImpacts();
        }

        private Vector3 GetnerateCommonImpact()
        {
            return _impacts.Aggregate(Vector3.Zero, (current, impactObject) => current + impactObject.Impact);
        }

        public void Move()
        {
            var diff = Target - Position;
            diff.Normalize();
            var distance = Vector3.Distance(Target, Position);
            if (MovementSpeed * Time.DeltaTime > distance)
            {
                Position = Target;
            }
            else
            {
                Velocity = diff * MovementSpeed;
            }
        }

        public void Rotate()
        {
            var sign = GetSignOfAngleToTarget();
            var angle = GetAngleToTarget();
            var result = RotationSpeed;
            var degreeSpeed = MathHelper.ToDegrees(RotationSpeed) * Time.DeltaTime;

            if (degreeSpeed > angle)
            {
                result = MathHelper.ToRadians(angle / Time.DeltaTime);
            }

            PhysicsObject.AngularVelocity = new Vector3(0, sign * result, 0);
        }

        private int GetSignOfAngleToTarget()
        {
            var current = PhysicsObject.OrientationMatrix.Forward;
            var target = Target - Position;
            target.Normalize();

            var crossed = Vector3.Cross(target, current);
            return Math.Sign(crossed.Y);
        }

        public float GetAngleToTarget()
        {
            var current = PhysicsObject.OrientationMatrix.Forward;
            var target = Target - Position;
            target.Normalize();

            return 180 - MathHelper.ToDegrees((float)Math.Acos(Vector3.Dot(target, current)));
        }

        public void ClearImpacts()
        {
            _impacts.Clear();
        }

        public void CreatePhysicalObject()
        {
            var capsule = new Capsule(GetRandomPosition(), 1.7f, 0.5f, 10);
            var inertia = capsule.LocalInertiaTensor;
            inertia.M11 = 0;
            inertia.M12 = 0;
            inertia.M13 = 0;
            inertia.M21 = 0;
            inertia.M22 = 0;
            inertia.M23 = 0;
            capsule.LocalInertiaTensor = inertia;

            capsule.Tag = _playerController;
            var matrix = capsule.OrientationMatrix;
            matrix.Forward = Vector3.Zero;
            capsule.OrientationMatrix = matrix;
            PhysicsObject = capsule;
            capsule.LookAt(new Vector3(0, capsule.Position.Y, 0));
            AddToSpace(capsule);
        }

        private Vector3 GetRandomPosition()
        {
            var x = -10 + TrueRandom.GetRandomNumber(0, 20);
            var y = 1;
            var z = -10 + TrueRandom.GetRandomNumber(0, 20);
            return new Vector3(x, y, z);
        }

        public void StopRotation()
        {
            PhysicsObject.AngularVelocity = Vector3.Zero;
        }

        public void Reset()
        {
            var space = _playerController.Space;
            var newPosition = GetRandomPosition();
            _target = newPosition;
            PhysicsObject.Position = newPosition;
            if (!space.Entities.Contains(PhysicsObject))
            {
                PhysicsObject.LinearVelocity = Vector3.Zero;
                StopRotation();
                space.Add(PhysicsObject);
            }

            PhysicsObject.LookAt(new Vector3(0, PhysicsObject.Position.Y, 0));

            _impacts.Clear();
        }

        private void AddToSpace(Entity entity)
        {
            _playerController.Space.Add(entity);
        }

        public void RemoveFromSpace()
        {
            _playerController.Space.Remove(PhysicsObject);
        }
    }
}