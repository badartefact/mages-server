﻿using BEPUutilities;
using GameServer.Configuration;

public class ImpactObject
{
    public Vector3 Impact
    {
        get;
        private set;
    }

    private float ImpactConsumeSpeed
    {
        get
        {
            return ConfigurationManager.Instance.Data.Physics.ImpactConsumeSpeed;
        }
    }

    public bool Dead
    {
        get
        {
            return Impact == Vector3.Zero;
        }
    }

    public ImpactObject(Vector3 direction)
    {
        Impact = direction;
    }

    public ImpactObject(Vector3 direction, float force)
    {
        direction.Y = 0;
        Impact = direction * force;
    }

    public void ConsumeImpact()
    {
        Impact = Vector3.Lerp(Impact, Vector3.Zero, ImpactConsumeSpeed * Time.DeltaTime);
    }
}