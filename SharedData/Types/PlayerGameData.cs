﻿using ProtoBuf;

namespace SharedData.Types
{
    [ProtoContract]
    public class PlayerGameData
    {
        [ProtoMember(1)]
        public string Name { get; set; }

        [ProtoMember(2)]
        public bool IsReady { get; set; }

        [ProtoMember(3)]
        public bool IsConnectedToLobby { get; set; }
    }
}