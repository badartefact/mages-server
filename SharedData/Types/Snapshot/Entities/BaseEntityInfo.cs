﻿using SharedData.Enums;
using ProtoBuf;

namespace SharedData.Types.Snapshot.Entities
{
    [ProtoContract]
    [ProtoInclude(100, typeof(CharacterEntityInfo))]
    [ProtoInclude(101, typeof(SpellEntity))]
    public class BaseEntity
    {
        [ProtoMember(1, IsRequired = true)]
        public string Id { get; set; }

        [ProtoMember(2, IsRequired = true)]
        public TVector3 Position { get; set; }

        [ProtoMember(3, IsRequired = true)]
        public TQuaternion Orientation { get; set; }

        [ProtoMember(4, IsRequired = true)]
        public string Name { get; set; }

        [ProtoMember(5, IsRequired = true)]
        public EntityType Type { get; set; }
    }
}