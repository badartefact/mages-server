﻿using BEPUutilities;
using GameServer.Entities;
using SharedData.Enums;
using System;

namespace GameServer.Physics.Controllers
{
    public class CharacterEntityController
    {
        private bool _isMoving;
        private bool _isPreparingToCast;

        // private bool _isRotating;
        private bool _isCasting;

        private bool _isNotifiedAboutDeath;
        private const float StopDistance = 0.1f;

        private readonly PlayerController _playerController;
        private SpellName _currentSpell;

        private readonly HpManager _hpManager;

        private DateTime _totalCastingTime;
        private DateTime _castingStarTime;

        public bool IsRunning
        {
            get
            {
                return _isMoving;
            }
        }

        public bool IsCasting
        {
            get
            {
                return _isCasting;
            }
        }

        public int CastingTime
        {
            get { return _isCasting ? (DateTime.Now - _castingStarTime).Milliseconds : 0; }
        }

        public float HitPoints
        {
            get
            {
                return _hpManager.HitPoints;
            }
        }

        public Vector3 Target
        {
            get
            {
                return _playerController.MovementController.Target;
            }
        }

        public Vector3 Position
        {
            get
            {
                return _playerController.MovementController.Position;
            }
            set
            {
                _playerController.MovementController.Position = value;
            }
        }

        public Quaternion Rotation
        {
            get
            {
                return _playerController.MovementController.Rotation;
            }
            set
            {
                _playerController.MovementController.Rotation = value;
            }
        }

        public CharacterEntityController(PlayerController controller)
        {
            _playerController = controller;
            _hpManager = new HpManager();
        }

        public void Update()
        {
            _playerController.MovementController.Velocity = new Vector3(0, _playerController.MovementController.Velocity.Y, 0);

            if (_isCasting)
            {
                if (DateTime.Now > _totalCastingTime)
                {
                    Cast();
                    _isCasting = false;
                }
            }
            else
            {
                CheckDistanceToTarget();

                if (_isPreparingToCast)
                    CheckSpellRotation();

                if (_isMoving && CheckMoveFightRotation())
                    MoveCharacter();
            }

            CheckDeath();
        }

        private void CheckSpellRotation()
        {
            var rotationIsOk = true;
            if (_playerController.CombatManager.SpellNeedRotation(_currentSpell))
            {
                rotationIsOk = CheckMoveFightRotation();
            }

            if (rotationIsOk)
            {
                _isPreparingToCast = false;
                _isMoving = false;
                _isCasting = true;
                SetCastingTime();
            }
        }

        private void SetCastingTime()
        {
            var spellData = _playerController.SpellManagerController.GetSpellByName(_currentSpell);
            _totalCastingTime = DateTime.Now.AddMilliseconds(spellData.CastingTime);
            _castingStarTime = DateTime.Now;
        }

        private void CheckDeath()
        {
            if (_hpManager.IsDead && !_isNotifiedAboutDeath)
            {
                _isNotifiedAboutDeath = true;
                _playerController.MovementController.RemoveFromSpace();
            }
        }

        private void MoveCharacter()
        {
            _playerController.MovementController.Move();
        }

        private void CheckDistanceToTarget()
        {
            var distance = Vector3.Distance(_playerController.MovementController.Target, _playerController.MovementController.Position);

            if (distance < StopDistance)
            {
                _isMoving = false;
                //_isRotating = false;
            }
        }

        public bool CheckMoveFightRotation()
        {
            var angle = _playerController.MovementController.GetAngleToTarget();

            if (angle > 1)
            {
                //_isRotating = true;
                _playerController.MovementController.Rotate();
            }

            if (angle > 10)
            {
                return false;
            }
            _playerController.MovementController.StopRotation();
            //_isRotating = false;
            return true;
        }

        public void StopMoving()
        {
            _isMoving = false;
        }

        public void MoveTo(Vector3 target)
        {
            if (_isCasting || _isPreparingToCast) return;
            _playerController.MovementController.Target = target;
            _isMoving = true;
        }

        public void Hold()
        {
            _isMoving = false;
            // _isRotating = false;
            _isPreparingToCast = false;
            _isCasting = false;
        }

        public void DoMagic(SpellName spell, Vector3 target)
        {
            if (_isCasting || _isPreparingToCast || !CanCastSkill(spell) || spell == SpellName.None) return;

            if (_playerController.SpellManagerController.SpellNeedsTargeting(spell))
                _playerController.MovementController.Target = target;
            _isPreparingToCast = true;
            _currentSpell = spell;
        }

        private bool CanCastSkill(SpellName spellName)
        {
            if (!_playerController.SpellManagerController.IsExists(spellName))
            {
                return false;
            }
            if (!_playerController.SpellManagerController.IsReady(spellName))
            {
                return false;
            }
            return true;
        }

        private void Cast()
        {
            _isPreparingToCast = false;
            _isMoving = false;

            switch (_currentSpell)
            {
                case SpellName.Fireball:
                    _playerController.CombatManager.ThrowFireball();
                    break;

                case SpellName.Teleport:
                    _playerController.CombatManager.Teleport();
                    break;

                case SpellName.Push:
                    _playerController.CombatManager.Push();
                    break;

                case SpellName.TpCharge:
                    _playerController.CombatManager.TpCharge();
                    break;

                case SpellName.Lighting:
                    _playerController.CombatManager.ThrowLighting();
                    break;

                case SpellName.Explode:
                    _playerController.CombatManager.Explode();
                    break;

                case SpellName.MagicChaser:
                    _playerController.CombatManager.ThrowMagicChaser();
                    break;
            }

            _playerController.SpellManagerController.StartSpellCooldown(_currentSpell);
        }

        public void AddImpact(ImpactObject impact)
        {
            _playerController.MovementController.AddImpact(impact);
        }

        public void AddImpact(Vector3 direction, float force)
        {
            var impact = new ImpactObject(direction, force);
            _playerController.MovementController.AddImpact(impact);
        }

        public void RemoveHp(float damage)
        {
            _hpManager.RemoveHp(damage);
        }

        public bool IsDead
        {
            get { return _hpManager.IsDead; }
        }

        public void Reset()
        {
            _isNotifiedAboutDeath = false;
            _hpManager.HitPoints = 100;
        }
    }
}