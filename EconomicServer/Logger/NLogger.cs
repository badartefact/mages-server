using SharedData.Logger;
using NLog;

namespace EconomicServer.Logger
{
    public enum LoggingLevel
    {
        Trace,
        Debug,
        Info,
        Warn,
        Error,
        Fatal,
        AllDisabled
    }

    public class NLogger : ILogger
    {
        #region Private members

        private readonly NLog.Logger _log;

        #endregion Private members

        #region Constructor

        public NLogger(string name)
            : this(LogManager.GetLogger(name))
        {
        }

        public NLogger(NLog.Logger log)
        {
            _log = log;
        }

        #endregion Constructor

        #region ILogger

        public bool IsEnabled(LoggingLevel level)
        {
            string levelName = level == LoggingLevel.AllDisabled ? "Off" : level.ToString();
            LogLevel nlogLevel = LogLevel.FromString(levelName);
            return _log.IsEnabled(nlogLevel);
        }

        public void Trace(string message)
        {
            _log.Trace(message);
        }

        public void Trace(string message, params object[] args)
        {
            _log.Trace(message, args);
        }

        public void Debug(string message)
        {
            _log.Debug(message);
        }

        public void Debug(string message, params object[] args)
        {
            _log.Debug(message, args);
        }

        public void Info(string message)
        {
            _log.Info(message);
        }

        public void Info(string message, params object[] args)
        {
            _log.Info(message, args);
        }

        public void Warn(string message)
        {
            _log.Warn(message);
        }

        public void Warn(string message, params object[] args)
        {
            _log.Warn(message, args);
        }

        public void Error(string message)
        {
            _log.Error(message);
        }

        public void Error(string message, params object[] args)
        {
            _log.Error(message, args);
        }

        public void Fatal(string message)
        {
            _log.Fatal(message);
        }

        public void Fatal(string message, params object[] args)
        {
            _log.Fatal(message, args);
        }

        #endregion ILogger
    }
}